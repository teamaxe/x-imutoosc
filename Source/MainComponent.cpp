	/*
  ==============================================================================

    This file was auto-generated!

  ==============================================================================
*/

#include "MainComponent.h"



//==============================================================================
MainContentComponent::MainContentComponent (xImuSensor& x, OscConnection& o)
 :  ximu (x),
    osc (o),
    serialControls (ximu.getSerialPort()),
    oscControls (osc),
    streamText ("---")
{
    addAndMakeVisible (serialControls);
    addAndMakeVisible (oscControls);
    addAndMakeVisible (streamTextLabel);
    addAndMakeVisible (imuButtons);
    addAndMakeVisible (inertialButtons);
    addAndMakeVisible (digitalButtons);
    addAndMakeVisible (analogueButtons);
    
    ximu.addListener (this);
    
    startTimer (100);
    
    setSize (400, 200);
}

MainContentComponent::~MainContentComponent()
{
    ximu.removeListener (this);
}

void MainContentComponent::paint (Graphics& g)
{

}

void MainContentComponent::resized()
{
    Rectangle<int> r (getLocalBounds());
    const int height = 20;
    serialControls.setBounds (r.removeFromTop (height));
    r.removeFromTop (height);
    imuButtons.setBounds (r.removeFromTop (height).withTrimmedLeft (height).withTrimmedRight (height));
    r.removeFromTop (height);
    inertialButtons.setBounds (r.removeFromTop (height).withTrimmedLeft (height).withTrimmedRight (height));
    r.removeFromTop (height);
    digitalButtons.setBounds (r.removeFromTop (height).withTrimmedLeft (height).withTrimmedRight (height));
    r.removeFromTop (height);
    analogueButtons.setBounds (r.removeFromTop (height).withTrimmedLeft (height).withTrimmedRight (height));
    
    Rectangle<int> w (r.removeFromBottom (height));
    streamTextLabel.setBounds(w.removeFromLeft (w.getWidth() / 2));
    oscControls.setBounds (w);
    
}

void MainContentComponent::xIMUCalInertialMagData (xImuSensor* source, CalInertialMagData* dataPacket)
{
    OscMessage mes;
    
    if (inertialButtons.getState())
    {
        if (inertialButtons.getAccelerometerState())
        {
            const std::vector<float>& acc (dataPacket->getAccelerometer());
            mes.setAddress ("/accelerometer");
            for (int i = 0; i < 3; i++)
                mes.addFloat32Argument (acc[i]);
            osc.write (mes, oscControls.getIP(), oscControls.getPort());
        }
        
        if (inertialButtons.getGyroscopeState())
        {
            const std::vector<float>& gyro (dataPacket->getGyroscope());
            mes.clear();
            mes.setAddress ("/gyroscope");
            for (int i = 0; i < 3; i++)
                mes.addFloat32Argument (gyro[i]);
            osc.write (mes, oscControls.getIP(), oscControls.getPort());
        }
        
        if (inertialButtons.getMagnetometerState())
        {
            const std::vector<float>& mag (dataPacket->getMagnetometer());
            mes.clear();
            mes.setAddress ("/magnetometer");
            for (int i = 0; i < 3; i++)
                mes.addFloat32Argument (mag[i]);
            osc.write (mes, oscControls.getIP(), oscControls.getPort());
        }
    }
    
    if (! mes.isEmpty())
    {
        SpinLock::ScopedLockType sl (lock);
        streamText = mes.asString();
    }
}

void MainContentComponent::xIMUQuaternionData (xImuSensor* source, QuaternionData* dataPacket)
{
    OscMessage mes;
    
    if (imuButtons.getState())
    {
        if (imuButtons.getQuaternionState())
        {
            const std::vector<float>& quat (dataPacket->getQuaternion());
            mes.setAddress ("/quaternion");
            for (int i = 0; i < 4; i++)
                mes.addFloat32Argument (quat[i]);
            osc.write (mes, oscControls.getIP(), oscControls.getPort());
        }
        
        if (imuButtons.getRotationState())
        {
            std::vector<float> rotation;
            dataPacket->ConvertToRotationMatrix (rotation);
            mes.clear();
            mes.setAddress ("/rotation");
            for (int i = 0; i < 9; i++)
                mes.addFloat32Argument (rotation[i]);
            osc.write (mes, oscControls.getIP(), oscControls.getPort());
        }
        
        if (imuButtons.getEulerState())
        {
            std::vector<float> euler;
            dataPacket->ConvertToEulerAngles (euler);
            mes.clear();
            mes.setAddress ("/euler");
            for (int i = 0; i < 3; i++)
                mes.addFloat32Argument (euler[i]);
            osc.write (mes, oscControls.getIP(), oscControls.getPort());
        }
    }
    
    if (! mes.isEmpty())
    {
        SpinLock::ScopedLockType sl (lock);
        streamText = mes.asString();
    }
}

void MainContentComponent::xIMUDigitalIOdata (xImuSensor* source, DigitalIOdata* dataPacket)
{
    
    if (digitalButtons.getState())
    {
        const unsigned char bits = dataPacket->getState().getPortBitsAsUnsignedChar();
        OscMessage mes ("/inputs");
        for (int i = 0; i < 4; i++)
        {
            mes.addInt32Argument ((bits & (0x1 << i)) > 0);
        }
        osc.write (mes, oscControls.getIP(), oscControls.getPort());
        SpinLock::ScopedLockType sl (lock);
        streamText = mes.asString();
    }
}

void MainContentComponent::xIMURawAnalogueData (xImuSensor* source, RawAnalogueInputData* dataPacket)
{
    if (digitalButtons.getState())
    {
        OscMessage mes ("/inputs");
        for (int i = 0; i < 4; i++)
        {
            mes.addFloat32Argument (dataPacket->getPort (i) / 4093.f);
        }
        osc.write (mes, oscControls.getIP(), oscControls.getPort());
        SpinLock::ScopedLockType sl (lock);
        streamText = mes.asString();
        DBG (mes.asString());
    }
}

void MainContentComponent::timerCallback()
{
    String localText;
    {
        SpinLock::ScopedLockType sl (lock);
        localText = streamText;
    }
    streamTextLabel.setText (localText, dontSendNotification);
    streamTextLabel.repaint();
}
