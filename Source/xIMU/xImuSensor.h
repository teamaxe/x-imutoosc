//
//  xImuSensor.h
//  x-IMUtoOSC
//
//  Created by Tom Mitchell on 18/10/2015.
//
//

#ifndef H_XIMUSENSOR
#define H_XIMUSENSOR

#include "../../JuceLibraryCode/JuceHeader.h"
#include "../serial/SerialPort.h"
#include "xIMUapi/PacketConstruction.h"

class xImuSensor : private SerialPort::Listener
{
public:
    xImuSensor() : receiveBufferIndex (0), totalPacketsRead (0)
    {
        sp.addListener (this);
    }
    
    ~xImuSensor()
    {
        sp.removeListener (this);
    }
    
    void parseData (const unsigned char* const data, const unsigned char size);
    
    SerialPort& getSerialPort() {return sp;}
    
    /**
     a class for listeners to inherit from
     */
    class Listener
    {
    public:
        virtual ~Listener() {}
        virtual void xIMUDateTimeData (xImuSensor* source, DateTimeData* dataPacket)=0;
        virtual void xIMUCalBattThermData (xImuSensor* source, CalBattThermData* dataPacket)=0;
        virtual void xIMUCommandData (xImuSensor* source, CommandData* dataPacket)=0;
        virtual void xIMURawBattThermData (xImuSensor* source, RawBattThermData* dataPacket)=0;
        virtual void xIMUCalInertialMagData (xImuSensor* source, CalInertialMagData* dataPacket)=0;
        virtual void xIMUQuaternionData (xImuSensor* source, QuaternionData* dataPacket)=0;
        virtual void xIMUDigitalIOdata (xImuSensor* source, DigitalIOdata* dataPacket)=0;
        virtual void xIMUErrorData (xImuSensor* source, ErrorData* dataPacket)=0;
        virtual void xIMURawAnalogueData (xImuSensor* source, RawAnalogueInputData* dataPacket)=0;
        virtual void xIMURegisterData (xImuSensor* source, RegisterData* dataPacket)=0;
    };
    
    /** Adds a listener to recieve all the packets, only one listener for now */
    void addListener (xImuSensor::Listener* const listenerToAdd);
    
    /**
     Removes a listener
     */
    void removeListener (xImuSensor::Listener* const listenerToRemove);

    
    //SerialPort::Listener
    void serialDataRecieved (const String& /*portName*/, const uint8 byte) override;
    void serialConnectionError(const unsigned int errorCode) override {};
    void serialPortTimeout() override {};
    
private:
    SerialPort sp;
    unsigned char receiveBuffer[256];
    unsigned char receiveBufferIndex;
    unsigned int totalPacketsRead;
    
    CriticalSection listenerLock;
    ListenerList <Listener> listeners;
};



#endif /* defined(__x_IMUtoOSC__xImuSensor__) */
