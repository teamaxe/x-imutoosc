//
//  xImuSensor.cpp
//  x-IMUtoOSC
//
//  Created by Tom Mitchell on 18/10/2015.
//
//

#include "xImuSensor.h"

void xImuSensor::serialDataRecieved (const String& /*portName*/, const uint8 byte)
{
    //DBG ((char)byte);
    
    receiveBuffer[receiveBufferIndex++] = byte;              // add new byte to buffer
    if ((byte & 0x80) == 0x80)                               // if new byte was packet framing char
    {
        parseData (&receiveBuffer[0], receiveBufferIndex);
        receiveBufferIndex = 0;                              // reset buffer.
    }
    
}

void xImuSensor::addListener(xImuSensor::Listener* const listenerToAdd)
{
    if (listenerToAdd != 0)
    {
        ScopedLock sl(listenerLock);
        listeners.add(listenerToAdd);
        //let the new listener know the current state of the connection
        //listenerToAdd->xIMUConnectionStateChanged(this, xIMUPort->isReading());
    }
}

void xImuSensor::removeListener(xImuSensor::Listener* const listenerToRemove)
{
    if (listenerToRemove != 0)
    {
        ScopedLock sl(listenerLock);
        listeners.remove(listenerToRemove);
    }
}

void xImuSensor::parseData (const unsigned char* const data, const unsigned char size)
{
    totalPacketsRead++;
    
    xIMUdata *dataObject = NULL;
    
    dataObject = PacketConstruction::DeconstructPacket(data, size);
    
    if (dataObject != NULL)
    {
        ScopedLock sl(listenerLock);
        if (dynamic_cast<DateTimeData*>(dataObject) != NULL)
            listeners.call (&Listener::xIMUDateTimeData, this, static_cast<DateTimeData*> (dataObject));
        else if (dynamic_cast<CalBattThermData*>(dataObject) != NULL)
            listeners.call (&Listener::xIMUCalBattThermData, this, static_cast<CalBattThermData*> (dataObject));
        else if (dynamic_cast<CommandData*>(dataObject) != NULL)
            listeners.call (&Listener::xIMUCommandData, this, static_cast<CommandData*> (dataObject));
        else if (dynamic_cast<RawBattThermData*>(dataObject) != NULL)
            listeners.call (&Listener::xIMURawBattThermData, this, static_cast<RawBattThermData*> (dataObject));
        else if(dynamic_cast<CalInertialMagData*>(dataObject) != NULL)
            listeners.call (&Listener::xIMUCalInertialMagData, this, static_cast<CalInertialMagData*> (dataObject));
        else if (dynamic_cast<QuaternionData*>(dataObject) != NULL)
            listeners.call (&Listener::xIMUQuaternionData, this, static_cast<QuaternionData*> (dataObject));
        else if (dynamic_cast<DigitalIOdata*>(dataObject) != NULL)
            listeners.call (&Listener::xIMUDigitalIOdata, this, static_cast<DigitalIOdata*> (dataObject));
        else if (dynamic_cast<ErrorData*>(dataObject) != NULL)
            std::cout << ((ErrorData *)dataObject)->GetMessage() << "\n";
        //listeners.call(&Listener::xIMUErrorData, this, static_cast<ErrorData*> (dataObject));
        else if (dynamic_cast<RawAnalogueInputData *>(dataObject) != NULL)
            listeners.call (&Listener::xIMURawAnalogueData, this, static_cast<RawAnalogueInputData*> (dataObject));
        else if(dynamic_cast<RegisterData*>(dataObject) != NULL)
            listeners.call (&Listener::xIMURegisterData, this, static_cast<RegisterData*> (dataObject));
        else if (dynamic_cast<PWMoutputData*>(dataObject) != NULL)
            ;//Do Nothing
        else
            std::cerr << "unrecognised packet\n";
        
        delete dataObject;  //this is a bit messey!
    }
    else
    {
        std::cerr << "Failed to Deconstruct Packet\n";
    }
}
