﻿#ifndef H_PWMOUTPUTDATA
#define H_PWMOUTPUTDATA

#include "xIMUdata.h"
#include <sstream>

//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;
//
//namespace x_IMU_API
//{
/// <summary>
/// PWM output data class.
//*/
class PWMoutputData : public xIMUdata
{
public:
    /**
    /// Gets or sets PWM channel AX0 duty cycle as a percentage.  Value must be between 0 and 1.
    */
    float getAX0() const {return privAX0;}
    void setAX0(const float value)
    {
        if(checkDutyCycle(value))
            privAX0 = value; 
    }
    
    /**
    /// Gets or sets PWM channel AX2 duty cycle as a percentage.  Value must be between 0 and 1.
    */
    float getAX2() const {return privAX2;}
    void setAX2(const float value)
    {
        if(checkDutyCycle(value))
            privAX2 = value; 
    }
    
    /**
    /// Gets or sets PWM channel AX4 duty cycle as a percentage.  Value must be between 0 and 1.
    */
    float getAX4() const {return privAX4;}
    void setAX4(const float value)
    {
        if(checkDutyCycle(value))
            privAX4 = value; 
    }
    
    /**
    /// Gets or sets PWM channel AX6 duty cycle as a percentage.  Value must be between 0 and 1.
    */
    float getAX6() const {return privAX6;}
    void setAX6(const float value)
    {
        if(checkDutyCycle(value))
            privAX6 = value; 
    }
    
    /**
    /// Initialises a new instance of the <see cref="PWMoutputData"/> class.
    */
    PWMoutputData()
    {
        privAX0 = 0.f;
        privAX2 = 0.f;
        privAX4 = 0.f;
        privAX6 = 0.f;
    }
    
    /**
    /// Initialises a new instance of the <see cref="PWMoutputData"/> class.
    */
    /// <param name="AX0">
    /// PWM channel AX0 duty cycle as a percentage
    /// </param>
    /// <param name="AX2">
    /// PWM channel AX2 duty cycle as a percentage
    /// </param>
    /// <param name="AX4">
    /// PWM channel AX4 duty cycle as a percentage
    /// </param>
    /// <param name="AX6">
    /// PWM channel AX6 duty cycle as a percentage
    /// </param>
    PWMoutputData(float _AX0, float _AX2, float _AX4, float _AX6)
    {
        privAX0 = _AX0;
        privAX2 = _AX2;
        privAX4 = _AX4;
        privAX6 = _AX6;
    }
    
    /**
    /// Converts data to string of Comma Separated Variables.
    */
    /// <returns>
    /// CSV line.
    /// </returns>
    void ConvertToCSV(std::string &pwmString)
    {
        std::stringstream ss;
        ss << privAX0 << "," << privAX2 << "," << privAX4 << "," << privAX6;
        pwmString = ss.str();
    }
    
private: 
    float privAX0;
    float privAX2;
    float privAX4;
    float privAX6;
    
    //check valid duty value
    bool checkDutyCycle(float dutyCycle) const
    {
        if((dutyCycle < 0.f) || (dutyCycle > 1.f)) 
        {
            return false;
        }
        return true;
    }
    
};
//}
#endif //H_PWMOUTPUTDATA


