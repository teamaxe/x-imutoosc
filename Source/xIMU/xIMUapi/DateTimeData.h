﻿#ifndef H_DATEANDTIMEDATA
#define H_DATEANDTIMEDATA

//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;

//namespace x_IMU_API
//{
    /**
    /// Date/Time data class.
    */
class DateTimeData : public xIMUdata
{
public:
    /**
    /// Gets or sets the year. Must be of value 2000 2099.
    */
    /// <exception cref="System.Exception">
    /// Thrown if invalid year specified.
    /// </exception>
    
    int getYear() const {return privYear;}
    void setYear(int newYear) 
    {
        if ((newYear < 2000) || (newYear > 2099))
            return;
            //throw new Exception("Year value must 2000 to 2099");
        
        privYear = newYear;
    }
    
    /**
    /// Gets or sets the month. Must be of value 1 to 12.
    */
    /// <exception cref="System.Exception">
    /// Thrown if invalid month specified.
    /// </exception>
    
    int getMonth()  const {return privMonth;}
    void setMonth(int newMonth) 
    {
        
        if ((newMonth < 1) || (newMonth > 12))
            return;
            //throw new Exception("Month value must 1 to 12");
        privMonth = newMonth;
    }
    
    /**
    /// Gets or sets the day. Must be of value 1 to 31.
    */
    /// <exception cref="System.Exception">
    /// Thrown if invalid day specified.
    /// </exception>
    int getDay() const {return privDay;}
    void setDay(int newDay)
    {
        if ((newDay < 1) || (newDay > 31))
            return;
            //throw new Exception("Day value must 1 to 31");
    
        privDay = newDay;
    }
    
    /**
    /// Gets or sets the hours. Must be of value 0 to 23.
    */
    /// <exception cref="System.Exception">
    /// Thrown if invalid hours specified.
    /// </exception>
    int getHours() const {return privHours;}
    void setHours(int newHours)
    {
        if ((newHours < 0) | (newHours > 23))
            return;
            //throw new Exception("Hours value must 00 to 23");
        
        privHours = newHours;
    }
    
    
    /**
    /// Gets or sets the minutes. Must be of value 0 to 59.
    */
    /// <exception cref="System.Exception">
    /// Thrown if invalid minutes specified.
    /// </exception>
    
    int getMinutes() const {return privMinutes;}
    void setMinutes(int newMinutes)
    {

        if ((newMinutes < 0) || (newMinutes > 59))
            return;
            //throw new Exception("Minutes value must 0 to 59");
        privMinutes = newMinutes;
    }
    
    
    /**
    /// Gets or sets the seconds. Must be of value 0 to 59.
    */
    /// <exception cref="System.Exception">
    /// Thrown if invalid seconds specified.
    /// </exception>
    int getSeconds() const {return privSeconds;}
    void setSeconds(int newSeconds)
    {
        if ((newSeconds < 0) | (newSeconds > 59))
            return;
            //throw new Exception("Seconds value must 0 to 59");
        
        privSeconds = newSeconds;
    }
    
    /**
    /// Gets or sets a <see cref="DateTime"/> object representation of data.
    */
//    public DateTime DateTimeObject
//    {
//        get
//        {
//            return new DateTime(Year, Month, Date, Hours, Minutes, Seconds);
//        }
//        set
//        {
//            Year = value.Year;
//            Month = value.Month;
//            Date = value.Day;
//            Hours = value.Hour;
//            Minutes = value.Minute;
//            Seconds = value.Second;
//        }
//    }
    
    
    /**
    /// Initialises a new instance of the <see cref="DateTimeData"/> class.
    */
    DateTimeData()
    {
        privYear = 2000;
        privMonth = 1;
        privDay = 1;
        privHours = 0;
        privMinutes = 0;
        privSeconds = 0;
    }
    
    /**
    /// Initialises a new instance of the <see cref="DateTimeData"/> class.
    */
    /// <param name="dateTime">
    /// <see cref="DateTime"/> object.
    /// </param>
    
    
    /**
    /// Initialises a new instance of the <see cref="DateTimeData"/> class.
    */
    /// <param name="year">
    /// Year. Must be of value 2000 2099.
    /// </param>
    /// <param name="month">
    /// Month. Must be of value 1 to 12.
    /// </param>
    /// <param name="date">
    /// Date. Must be of value 1 to 31.
    /// </param>
    /// <param name="hours">
    /// Hours. Must be of value 0 to 23.
    /// </param>
    /// <param name="minutes">
    /// Minutes. Must be of value 0 to 59.
    /// </param>
    /// <param name="seconds">
    /// Seconds. Must be of value 0 to 59.
    /// </param>
    DateTimeData(int year, int month, int day, int hours, int minutes, int seconds)
    :   privYear(year),
        privMonth(month),
        privDay(day),
        privHours(hours),
        privMinutes(minutes),
        privSeconds(seconds)
    {
        
    }
    
    /**
    /// Converts date/time data to string.
    */
    /// <returns>
    /// Date/time data as string.
    /// </returns>
    
    void ConvertToCSV(std::string &dateString)
    {
        std::stringstream ss;
        ss << privYear << "," << privMonth << "," << privDay << "," << privHours << "," << privMinutes << "," << privSeconds;
        dateString = ss.str();
    }
    
    
private:
    int privYear;
    int privMonth;
    int privDay;
    int privHours;
    int privMinutes;
    int privSeconds;
};
//}
#endif //H_DATEANDTIMEDATA