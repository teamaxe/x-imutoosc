﻿#ifndef H_PACKETENCODING
#define H_PACKETENCODING

#include <cmath>

//namespace xIMU_API
//{
    /**
    /// Packet encoding class.  Contains static methods for packet encoding and decoding.
    */
	class PacketEncoding
    {
	public:
        /**
        /// Encodes packet with consecutive right shifts so that the msb of each encoded byte is clear. The msb of the final byte is set to indicate the end of the packet.
        */
        /// <param name="decodedPacket">
        /// The decoded packet contents to be encoded.
        /// </param>
        /// <returns>
        /// The encoded packet.
        /// </returns> 
		
//		public static byte[] EncodePacket(byte[] decodedPacket)
//        {
//            byte[] encodedPacket = new byte[(int)(Math.Ceiling((((float)decodedPacket.Length * 1.125f)) + 0.125f))];
//            byte[] shiftRegister = new byte[encodedPacket.Length];
//            Array.Copy(decodedPacket, shiftRegister, decodedPacket.Length);     // copy encoded packet to shift register
//            for (int i = 0; i < encodedPacket.Length; i++)
//            {
//                shiftRegister = RightShiftByteArray(shiftRegister);             // right shift to clear msb of byte i
//                encodedPacket[i] = shiftRegister[i];                            // store encoded byte i
//                shiftRegister[i] = 0;                                           // clear byte i in shift register
//            }
//            encodedPacket[encodedPacket.Length - 1] |= 0x80;                    // set msb of framing byte
//            return encodedPacket;
//        }
		
		static unsigned int EncodePacket(unsigned char *encodedPacket, const unsigned char *decodedPacket, unsigned int decodedPacketSize)
        {
			unsigned int encodedPacketSize = (unsigned int)(std::ceil((((float)decodedPacketSize * 1.125f)) + 0.125f));
			
			unsigned char* shiftRegister = new unsigned char[encodedPacketSize];
			
			memcpy(shiftRegister, decodedPacket, decodedPacketSize);
			for (unsigned int i = 0; i < encodedPacketSize; i++)
            {
                RightShiftByteArray(shiftRegister, encodedPacketSize);             // right shift to clear msb of byte i
                encodedPacket[i] = shiftRegister[i];            // store encoded byte i
                shiftRegister[i] = 0;                           // clear byte i in shift register
            }
			encodedPacket[encodedPacketSize - 1] |= 0x80;                    // set msb of framing byte
			delete[] shiftRegister;
            return encodedPacketSize;
        }

        /**
        /// Decodes a packet with consecutive left shifts so that the msb of each encoded byte is removed.
        */
        /// <param name="encodedPacket">
        /// The endcoded packet to be decoded.
        /// </param>
        /// <returns>
        /// The decoded packet.
        /// </returns> 
        static unsigned int DecodePacket(const unsigned char *encodedPacket, unsigned char *decodedPacket, unsigned int encodedSize)
        {
			unsigned int decodedPacketSize = (int)(std::floor(((float)encodedSize - 0.125f) / 1.125f));
			
            unsigned char *shiftRegister = new unsigned char[encodedSize];
			
            for (int i = encodedSize - 1; i >= 0; i--)
            {
                shiftRegister[i] = encodedPacket[i];
                LeftShiftByteArray(shiftRegister, encodedSize);
            }
			memcpy(decodedPacket, shiftRegister, decodedPacketSize);
			
			delete[] shiftRegister;
            return decodedPacketSize;
        }
		
	private:
		/**
		 /// Right shifts a byte array by 1 bit.  The lsb of byte x becomes the msb of byte x+1.
		 */
        /// <param name="byteArray">
        /// The byte array to be right shifted.
        /// </param>
        /// <returns>
        /// The right shifted byte array.
        /// </returns> 
		static void RightShiftByteArray(unsigned char* byteArray, unsigned int byteArrayLength)
        {
            byteArray[byteArrayLength - 1] >>= 1;
            for (int i = byteArrayLength - 2; i >= 0; i--)
            {
                if ((byteArray[i] & 0x01) == 0x01) byteArray[i + 1] |= 0x80;
                byteArray[i] >>= 1;
            }
        }

        /**
        /// Left shifts a byte array by 1 bit.  The msb of byte x becomes the lsb of byte x-1.
        */
        /// <param name="byteArray">
        /// The byte array to be left shifted.
        /// </param>
        /// <returns>
        /// The left shifted byte array.
        /// </returns> 
        static void LeftShiftByteArray(unsigned char *byteArray, unsigned int byteArrayLength)
        {
            byteArray[0] <<= 1;
            for (int i = 1; i < byteArrayLength; i++)
            {
                if ((byteArray[i] & 0x80) == 0x80) byteArray[i - 1] |= 0x01;
                byteArray[i] <<= 1;
            }
        }
    };
//}

#endif //H_PACKETENCODING