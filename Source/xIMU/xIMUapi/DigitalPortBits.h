﻿#ifndef H_DIGITALPORTBITS
#define H_DIGITALPORTBITS

#include <vector>
#include <algorithm>

#include "xIMUdata.h"


//TM to do - overload the [] operator so this behaves like an array

//namespace xIMU_API
//{
/// <summary>
/// Port data class.
/// </summary>
class DigitalPortBits
{
public:
	/// <summary>
	/// Initializes a new instance of the <see cref="DigitalPortBits"/> class.
	/// </summary>
	DigitalPortBits()
	{
		initPortBits();
	}
	
	/// <summary>
	/// Initializes a new instance of the <see cref="DigitalPortBits"/> class.
	/// </summary>
	/// <param name="portByte">
	/// 8-bit port value.  lsb = AX0, msb = AX7.
	/// </param>
	DigitalPortBits(const std::vector<bool> initPortByte)
	{
		initPortBits();
		setPortBits(initPortByte);
	}
	
	/// <summary>
	/// Initializes a new instance of the <see cref="DigitalPortBits"/> class.
	/// </summary>
	/// <param name="portBits">
	/// 8-bit port value.  bits[0] = AX0 (lsb), bits[7] = AX7 (msb).
	/// </param>
	DigitalPortBits(const bool initPortByte[8])
	{
		initPortBits();
		setPortBits(initPortByte);
	}
	
	/// <summary>
	/// Initializes a new instance of the <see cref="DigitalPortBits"/> class.
	/// </summary>
	/// <param name="portBits">
	/// 8-bit port value.  bits[0] = AX0 (lsb), bits[7] = AX7 (msb).
	/// </param>
	DigitalPortBits(const unsigned char initPortByte)
	{
		initPortBits();
		setPortBits(initPortByte);
	}
	
	/**
	 Alocates and initialises the bits to false
	 */
	void initPortBits()
	{
		portBits.resize(8, false);
	}
	
	/// <summary>
	/// Gets or sets channel AX0.  lsb of 8-bit port.
	/// </summary>
	bool getAX0() const {return portBits[0];}
	void setAX0(const bool state){portBits[0] = state;}
	
	/// <summary>
	/// Gets or sets channel AX1.  2nd lsb of 8-bit port.
	/// </summary>
	
	bool getAX1() const {return portBits[1];}
	void setAX1(const bool state){portBits[1] = state;}
	
	/// <summary>
	/// Gets or sets channel AX2.  3rd lsb of 8-bit port.
	/// </summary>
	bool getAX2() const {return portBits[2];}
	void setAX2(const bool state){portBits[2] = state;}
	
	/// <summary>
	/// Gets or sets channel AX3.  4th lsb of 8-bit port.
	/// </summary>
	bool getAX3() const {return portBits[3];}
	void setAX3(const bool state){portBits[3] = state;}
	
	/// <summary>
	/// Gets or sets channel AX4.  5th lsb of 8-bit port.
	/// </summary>
	bool getAX4() const {return portBits[4];}
	void setAX4(const bool state){portBits[4] = state;}
	
	/// <summary>
	/// Gets or sets channel AX5.  6th lsb of 8-bit port.
	/// </summary>
	bool getAX5() const {return portBits[5];}
	void setAX5(const bool state){portBits[5] = state;}
	
	/// <summary>
	/// Gets or sets channel AX6.  7th lsb of 8-bit port.
	/// </summary>
	bool getAX6() const {return portBits[6];}
	void setAX6(const bool state){portBits[6] = state;}
	
	/// <summary>
	/// Gets or sets channel AX7.  msb of 8-bit port.
	/// </summary>
	bool getAX7() const {return portBits[7];}
	void setAX7(const bool state){portBits[7] = state;}
	
	bool getPortBit(const int bit) const
	{
		if (bit >= 0 && bit < 8) {
			return portBits[bit];
		}
		else 
		{
			return false;
		}
	}
	
	/// <summary>
	/// Gets 8-bit port value.  bits[0] = AX0 (lsb), bits[7] = AX7 (msb).
	/// </summary>
	std::vector<bool> getPortBitsAsVector() const {return portBits;}
	
	/// <summary>
	/// Sets 8-bit port value.  bits[0] = AX0 (lsb), bits[7] = AX7 (msb).
	/// </summary>
	void setPortBits(const std::vector<bool> newPortBits)
	{
		if (newPortBits.size() == 8) 
			copy(newPortBits.begin(), newPortBits.end(), portBits.begin());
	}
	
	/// <summary>
	/// Sets 8-bit port value.  bits[0] = AX0 (lsb), bits[7] = AX7 (msb).
	/// </summary>
	void setPortBits(const bool newPortBits[])
	{
		copy(newPortBits, newPortBits+8, portBits.begin());
	}
	
	
	/// <summary>
	/// Gets 8-bit port value.  lsb = AX0, msb = AX7.
	/// </summary>
	const unsigned char getPortBitsAsUnsignedChar() const
	{
		return (const unsigned char)((portBits[7] ? 0x80 : 0x00) |
							   (portBits[6] ? 0x40 : 0x00) |
							   (portBits[5] ? 0x20 : 0x00) |
							   (portBits[4] ? 0x10 : 0x00) |
							   (portBits[3] ? 0x08 : 0x00) |
							   (portBits[2] ? 0x04 : 0x00) |
							   (portBits[1] ? 0x02 : 0x00) |
							   (portBits[0] ? 0x01 : 0x00));
		
	}
	
	/// <summary>
	/// Sets 8-bit port value.  lsb = AX0, msb = AX7.
	/// </summary>
	void setPortBits(const unsigned char newPortBits)
	{
		portBits[7] = (newPortBits & 0x80) == 0x80;
		portBits[6] = (newPortBits & 0x40) == 0x40;
		portBits[5] = (newPortBits & 0x20) == 0x20;
		portBits[4] = (newPortBits & 0x10) == 0x10;
		portBits[3] = (newPortBits & 0x08) == 0x08;
		portBits[2] = (newPortBits & 0x04) == 0x04;
		portBits[1] = (newPortBits & 0x02) == 0x02;
		portBits[0] = (newPortBits & 0x01) == 0x01;
	}
	
private:
	std::vector<bool> portBits;
	
};
//}

#endif //H_DIGITALPORTBITS