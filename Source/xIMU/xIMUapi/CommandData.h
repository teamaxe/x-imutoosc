﻿#ifndef H_COMMANDDATA
#define H_COMMANDDATA

#include "xIMUdata.h"


/**
 /// Command data class.
 */
class CommandData : public xIMUdata
{
    
public:
    /**
     /// Initialises a new instance of the <see cref="CommandData"/> class.
     */
    CommandData() : privCommandCode(CommandCodes::NullCommand)
    {
    }
    
    /**
     /// Initialises a new instance of the <see cref="CommandData"/> class.
     */
    /// <param name="commandCode">
    /// Command code. See <see cref="CommandCodes"/>.
    /// </param>
    CommandData (CommandCodes::CommandCodes_ commandCode) : privCommandCode((unsigned int)commandCode)
    {
    }
    
    /**
     /// Initialises a new instance of the <see cref="CommandData"/> class.
     */
    /// <param name="commandCode">
    /// Command code. Must be defined in <see cref="CommandCodes"/>.
    /// </param>
    CommandData(const unsigned int commandCode) : privCommandCode (commandCode)
    {
    }
    
    /**
     /// Gets or sets the command code.  Must be defined in <see cref="CommandCodes"/>.
     */
    /// <exception cref="System.Exception">
    /// Thrown if invalid command code specifed.
    /// </exception>
    unsigned short getCommandCode() const {return privCommandCode;}
    
    void setCommandCode(const unsigned short newCommandCode) 
    {
        if (newCommandCode < CommandCodes::NumCommandCodes) 
        {
            privCommandCode = newCommandCode;
        }
        else 
        {
            std::cout << "CommandData: Invalid command code:" << newCommandCode << "\n";
        }
        
    }
    
    /**
     /// Gets message associated with command code.
     */
    /// <returns>
    /// Message.
    /// </returns>
    void getMessage(std::string& commandText)
    {
        switch (privCommandCode)
        {
            case ((int)CommandCodes::NullCommand): commandText = "Null command."; break;
            case ((int)CommandCodes::FactoryReset): commandText =  "Factory reset."; break;
            case ((int)CommandCodes::Reset): commandText =  "Reset."; break;
            case ((int)CommandCodes::Sleep): commandText = "Sleep."; break;
            case ((int)CommandCodes::ResetSleepTimer): commandText = "Reset sleep timer."; break;
            case ((int)CommandCodes::SampleGyroscopeAxisAt200dps): commandText = "Sample gyroscope axis at ±200 dps."; break;
            case ((int)CommandCodes::CalcGyroscopeSensitivity): commandText = "Calculate gyroscope sensitivity."; break;
            case ((int)CommandCodes::SampleGyroscopeBiasTemp1): commandText = "Sample gyroscope bias at temperature 1."; break;
            case ((int)CommandCodes::SampleGyroscopeBiasTemp2): commandText = "Sample gyroscope bias at temperature 2."; break;
            case ((int)CommandCodes::CalcGyroscopeBiasParameters): commandText = "Calculate gyroscope bias parameters."; break;
            case ((int)CommandCodes::SampleAccelerometerAxisAt1g): commandText = "Sample accelerometer axis at ±1 g."; break;
            case ((int)CommandCodes::CalcAccelerometerBiasAndSens): commandText = "Calculate accelerometer bias and sensitivity"; break;
            case ((int)CommandCodes::MeasureMagnetometerBiasAndSens): commandText = "Measure magnetometer bias and sensitivity."; break;
            case ((int)CommandCodes::AlgorithmInitialise): commandText = "Algorithm initialise."; break;
            case ((int)CommandCodes::AlgorithmTare): commandText = "Algorithm tare."; break;
            case ((int)CommandCodes::AlgorithmClearTare): commandText = "Algorithm clear tare."; break;
            case ((int)CommandCodes::AlgorithmInitialiseThenTare): commandText = "Algorithm initialise then tare."; break;
            default: commandText = ""; break;
        }
    }
    
private:
    unsigned short privCommandCode;
};

#endif //H_COMMANDDATA