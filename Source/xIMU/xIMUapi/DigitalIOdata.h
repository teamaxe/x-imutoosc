﻿#ifndef H_DIGITALIODATA
#define H_DIGITALIODATA

#include "DigitalPortBits.h"
#include "xIMUdata.h"
#include <sstream>

//namespace xIMU_API
//{
/// <summary>
/// Digital I/O data class.
/// </summary>
class DigitalIOdata : public xIMUdata
{
public:
	/// <summary>
	/// Initialises a new instance of the <see cref="DigitalIOdata"/> class.
	/// </summary>
	DigitalIOdata()
	{
	}
	
	/// <summary>
	/// Initialises a new instance of the <see cref="DigitalIOdata"/> class.
	/// </summary>
	/// <param name="state">
	/// State of the digital I/O port.  true (1) = logic high, false (0) = logic low.
	/// </param>
	DigitalIOdata(const DigitalPortBits state_) : state(state_)
	{

	}
	
	/// <summary>
	/// Initialises a new instance of the <see cref="DigitalIOdata"/> class.
	/// </summary>     
	/// <param name="direction">
	/// Direction of the digital I/O port.  true (1) = input, false (0) = output.
	/// </param>
	/// <param name="state">
	/// State of the digital I/O port.  true (1) = logic high, false (0) = logic low.
	/// </param>
	DigitalIOdata(const DigitalPortBits newDirection, const DigitalPortBits newState)
	{
		direction = newDirection;
		state = newState;
	}
	
	/// <summary>
	/// Direction of the digital I/O port.  true (1) = input, false (0) = output.
	/// </summary>
	DigitalPortBits getDirection() const {return direction;}
	
	/// <summary>
	/// State of the digital I/O port.  true (1) = logic high, false (0) = logic low.
	/// </summary>
	DigitalPortBits getState() const {return state;}
	void setState(DigitalPortBits &newState){state = newState;}
	
	/// <summary>
	/// Converts data to string of Comma Separated Variables.
	/// </summary>
	void ConvertToCSV(std::string &ioDataString) const
	{
		std::stringstream ss;
		for (int i = 0; i < 8; i++)
		{
			ss << (direction.getPortBit(i) ? "1" : "0") << ",";
		}
		for (int i = 0; i < 8; i++)
		{
			ss << (state.getPortBit(i) ? "1" : "0");
			if (i < 7) ss << ",";
		}
		
		ioDataString = ss.str();
	}
	

private:
	
	DigitalPortBits direction;
	DigitalPortBits state;
	
};
//}

#endif //H_DIGITALIODATA