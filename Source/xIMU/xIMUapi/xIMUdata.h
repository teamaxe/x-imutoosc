﻿#ifndef H_xIMUDATA
#define H_xIMUDATA

/**
 Abstract base class for data packets
 */
class xIMUdata 
{ 
public:
	virtual ~xIMUdata() {}
};

#endif //H_xIMUDATA