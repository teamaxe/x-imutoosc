﻿#ifndef H_FIXEDFLOAT
#define H_FIXEDFLOAT

//namespace xIMU_API
//{
    /**
    /// 16-bit fixed-point / floating-point conversion class.
    */
    //internal 
	class FixedFloat
    {
	public:
        /**
        /// Returns floating-point value from specified 16-bit fixed-point.
        */
        /// <param name="fixedValue">
        /// 16-bit fixed-point value.
        /// </param>
        /// <param name="q">
        /// Number of fraction bits. See <see cref="Qvals"/>.
        /// </param>
        /// <returns>
        /// Floating-point number.
        /// </returns>
        //public 
		static float FixedToFloat(short fixedValue, int q)
        {
            return ((float)(fixedValue) / (float)(1 << ((int)q)));
        }

        /**
        /// Returns 16-bit fixed-point value from specified floating-point value with saturation.
        */
        /// <param name="floatValue">
        /// Floating-point representation of 16-bit fixed-point value.
        /// </param>
        /// <param name="q">
        /// Number of fraction bits. See <see cref="Qvals"/>.
        /// </param>
        /// <returns>
        /// 16-bit fixed-point value.
        /// </returns>
        //public 
		static short FloatToFixed(float floatValue, int q)
        {
            int temp = (int)((floatValue) * (int)(1 << ((int)q)));
            if (temp > 32767) temp = 32767;
            else if (temp < -32768) temp = -32768;
            return (short)temp;
        }
    };
//}

#endif// H_FIXEDFLOAT
