﻿#ifndef H_CALINERTIALMAGDATA
#define H_CALINERTIALMAGDATA

#include "xIMUdata.h"
#include <vector>
#include <iomanip>
#include <sstream>

//namespace xIMU_API
//{
    /// <summary>
    /// Calibrated gyroscope, accelerometer and magnetometer data class.
    /// </summary>
    class CalInertialMagData : public xIMUdata
    {
	public:
		/// <summary>
        /// Initialises a new instance of the <see cref="CalInertialMagData"/> class.
        /// </summary>
		CalInertialMagData()
        {
			initMagData();
        }
        
        //Copy constructor
        CalInertialMagData(const CalInertialMagData& source)
        {
            initMagData();
            std::vector <float> copyVector;
            source.getAccelerometer(copyVector);
            setAccelerometer(copyVector);
            source.getGyroscope(copyVector);
            setGyroscope(copyVector);
            source.getMagnetometer(copyVector);
            setMagnetometer(copyVector);
        }
        //Assingment overload
        CalInertialMagData& operator=(const CalInertialMagData& source)
        {
            if (this == &source)
                return *this;
            
            std::vector <float> copyVector;
            source.getAccelerometer(copyVector);
            setAccelerometer(copyVector);
            source.getGyroscope(copyVector);
            setGyroscope(copyVector);
            source.getMagnetometer(copyVector);
            setMagnetometer(copyVector);
            
            return *this;            
        }

		
        /// <summary>
        /// Initialises a new instance of the <see cref="CalInertialMagData"/> class.
        /// </summary>
        /// <param name="gyroscope">
        /// Calibrated gyroscope data in degrees per second.  Elements 0, 1 and 2 represent the sensor x, y and z axes respectively.
        /// </param>
        /// <param name="accelerometer">
        /// Calibrated accelerometer data in 'g'.  Elements 0, 1 and 2 represent the sensor x, y and z axes respectively.
        /// </param>  
        /// <param name="magnetometer">
        /// Calibrated magnetometer data in Gauss.  Elements 0, 1 and 2 represent the sensor x, y and z axes respectively.
        /// </param>  
        CalInertialMagData(std::vector<float> &initGyroscope, std::vector<float> &initAccelerometer, std::vector<float> &initMagnetometer)
        {
			initMagData();
			
			if (initGyroscope.size() == 3) 
				copy(initGyroscope.begin(), initGyroscope.end(), gyroscope.begin());
			if (initAccelerometer.size() == 3) 
				copy(initAccelerometer.begin(), initAccelerometer.end(), accelerometer.begin());
			if (initMagnetometer.size() == 3) 
				copy(initMagnetometer.begin(), initMagnetometer.end(), magnetometer.begin());
		}
		
		void initMagData()
		{
			gyroscope.resize(3, 0.f);
			accelerometer.resize(3, 0.f);
			magnetometer.resize(3, 0.f);
		}
		
        /// <summary>
        /// Calibrated gyroscope data in degrees per second.  Elements 0, 1 and 2 represent the sensor x, y and z axes respectively.
        /// </summary>
		
		void getGyroscope(std::vector <float> &destGyroscope) const
		{
			destGyroscope.resize(3);
			copy(gyroscope.begin(), gyroscope.end(), destGyroscope.begin());
		}
        
        const std::vector<float>& getGyroscope() const
        {
            return gyroscope;
        }
		
		void setGyroscope(std::vector <float> &srcGyroscope)
		{
			if (srcGyroscope.size() == 3)
				copy(srcGyroscope.begin(), srcGyroscope.end(), gyroscope.begin());
		}
		
        /// <summary>
        /// Calibrated accelerometer data in 'g'.  Elements 0, 1 and 2 represent the sensor x, y and z axes respectively.
        /// </summary>
		void getAccelerometer(std::vector <float> &destAccelerometer) const
		{
			destAccelerometer.resize(3);
			copy(accelerometer.begin(), accelerometer.end(), destAccelerometer.begin());
		}
        
        const std::vector<float>&  getAccelerometer() const
        {
            return accelerometer;
        }
		
		void setAccelerometer(std::vector <float> &srcAccelerometer)
		{
			if (srcAccelerometer.size() == 3)
				copy(srcAccelerometer.begin(), srcAccelerometer.end(), accelerometer.begin());
		}

        /// <summary>
        /// Calibrated magnetometer data in Gauss.  Elements 0, 1 and 2 represent the sensor x, y and z axes respectively.
        /// </summary>
		void getMagnetometer(std::vector <float> &destMagnetometer) const
		{
			destMagnetometer.resize(3);
			copy(magnetometer.begin(), magnetometer.end(), destMagnetometer.begin());
		}
        
        const std::vector<float>&  getMagnetometer() const
        {
            return magnetometer;
        }
		
		void setMagnetometer(std::vector <float> &srcMagnetometer)
		{
			if (srcMagnetometer.size() == 3)
				copy(srcMagnetometer.begin(), srcMagnetometer.end(), magnetometer.begin());
		}
		
        /// <summary>
        /// Converts data to string of Comma Separated Variables.
        /// </summary>
        void ConvertToCSV(std::string &calInertialMagString) const
        {
			std::stringstream ss;
			
			ss << std::setprecision(2) <<	gyroscope[0]	 << "," << gyroscope[1]		<< "," << gyroscope[2]		<< "," <<
					accelerometer[0] << "," << accelerometer[1] << "," << accelerometer[2]	<< "," <<
					magnetometer[0]	 << "," << magnetometer[1]	<< "," << magnetometer[2];
			calInertialMagString = ss.str();
		}

        
	private:
		std::vector <float> gyroscope;
        std::vector <float> accelerometer;
        std::vector <float> magnetometer;
    };
//}

#endif //H_CALINERTIALMAGDATA
