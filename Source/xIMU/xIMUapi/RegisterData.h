﻿#ifndef H_REGISTERDATA
#define H_REGISTERDATA

/**
/// Register data class.
*/
class RegisterData : public xIMUdata
{
public:
    /**
    /// Gets or sets the 16-bit register address.  Must be defined in <see cref="RegisterAddresses"/>.
    */
    
    unsigned short int getAddress() const {return privAddress;}
    void setAddress(const unsigned short int newAddress)
    {
        if (newAddress < RegisterAddresses::NumRegisters) 
        {
            privAddress = newAddress;
        }
    }
    
    /**
    /// Gets or sets the 16-bit register value.
    */
    unsigned short int getValue() const {return privValue;}
    void setValue (const unsigned short int newValue)
    {
        privValue = newValue;
    }
    
    /**
    /// Gets or sets the floating-point representation of 16-bit register value when interpreted as fixed-point.
    */
    float getFloatValue() const {return FixedFloat::FixedToFloat((short int) privValue, (int)lookUpQval());}
    void setFloatValue(const float newFloatValue)
    {
        privValue = (unsigned short int) FixedFloat::FloatToFixed(newFloatValue, (int)lookUpQval());
    }
    
    /**
    /// Initialises a new instance of the <see cref="RegisterData"/> class.
    */
    explicit RegisterData() : privAddress(0), privValue(0)
    {
    }
    
    /**
    /// Initialises a new instance of the <see cref="RegisterData"/> class.
    */
    /// <param name="address">
    /// 16-bit register address. Must be defined in <see cref="RegisterAddresses"/>.
    /// </param>
    explicit RegisterData(const unsigned short int address) : privAddress(address), privValue(0)
    {
    }
    
    /**
    /// Initialises a new instance of the <see cref="RegisterData"/> class.
    */
    /// <param name="registerAddress">
    /// Register address. See <see cref="RegisterAddresses"/>.
    /// </param>
    explicit RegisterData(const RegisterAddresses::RegisterAddresses_ registerAddress) : privAddress ((unsigned short int)registerAddress), privValue (0)
    {
    }
    
    /**
    /// Initialises a new instance of the <see cref="RegisterData"/> class.
    */
    /// <param name="address">
    /// 16-bit register address. Must be defined in <see cref="RegisterAddresses"/>.
    /// </param>
    /// <param name="floatValue">
    /// Floating-point representation of 16-bit fixed-point value.
    /// </param>
    explicit RegisterData(unsigned short int address, float floatValue)
    :   privAddress (address), 
        privValue ((unsigned short int)FixedFloat::FloatToFixed(floatValue, lookUpQval(address)))
    {
    }
    
    /**
    /// Initialises a new instance of the <see cref="RegisterData"/> class.
    */
    /// <param name="registerAddress">
    /// Register address. See <see cref="RegisterAddresses"/>.
    /// </param>
    /// <param name="floatValue">
    /// Floating-point representation of 16-bit fixed-point value.
    /// </param>
    explicit RegisterData(RegisterAddresses::RegisterAddresses_ registerAddress, float floatValue)
    :   privAddress ((unsigned short int)registerAddress), 
        privValue ((unsigned short int)FixedFloat::FloatToFixed(floatValue, lookUpQval((unsigned short int)registerAddress)))
    {
        
    }
    
    /**
    /// Initialises a new instance of the <see cref="RegisterData"/> class.
    */
    /// <param name="address">
    /// 16-bit register address. Must be defined in <see cref="RegisterAddresses"/>.
    /// </param>
    /// <param name="value">
    /// 16-bit register value.
    /// </param>
    explicit RegisterData (unsigned short int address, unsigned short int value)
    :   privAddress (address), privValue (value)
    {
    }
    
    /**
    /// Returns <see cref="Qvals"/> associated with register address.
    */
    /// <returns>
    /// <see cref="Qvals"/> associated with register address.
    /// </returns>
    Qvals::Qvals_ lookUpQval() const
    {
        return lookUpQval(privAddress);
    }
    
    /**
    /// Returns <see cref="Qvals"/> associated with specified register address.
    */
    /// <param name="address">
    /// Register address.
    /// </param>
    /// <returns>
    /// <see cref="Qvals"/> associated with register address.
    /// </returns>
    /// <exception cref="System.Exception">
    /// Thrown if register address does not have associated <see cref="Qvals"/>.
    /// </exception>
    static Qvals::Qvals_ lookUpQval(unsigned short int address)
    {
        switch (address)
        {
            case ((unsigned short int)RegisterAddresses::BattSensitivity): return Qvals::BattSensitivity;
            case ((unsigned short int)RegisterAddresses::BattBias): return Qvals::BattBias;
            case ((unsigned short int)RegisterAddresses::ThermSensitivity): return Qvals::ThermSensitivity;
            case ((unsigned short int)RegisterAddresses::ThermBias): return Qvals::ThermBias;
            case ((unsigned short int)RegisterAddresses::GyroSensitivityX): return Qvals::GyroSensitivity;
            case ((unsigned short int)RegisterAddresses::GyroSensitivityY): return Qvals::GyroSensitivity;
            case ((unsigned short int)RegisterAddresses::GyroSensitivityZ): return Qvals::GyroSensitivity;
            case ((unsigned short int)RegisterAddresses::GyroSampledPlus200dpsX): return Qvals::GyroSampled200dps;
            case ((unsigned short int)RegisterAddresses::GyroSampledPlus200dpsY): return Qvals::GyroSampled200dps;
            case ((unsigned short int)RegisterAddresses::GyroSampledPlus200dpsZ): return Qvals::GyroSampled200dps;
            case ((unsigned short int)RegisterAddresses::GyroSampledMinus200dpsX): return Qvals::GyroSampled200dps;
            case ((unsigned short int)RegisterAddresses::GyroSampledMinus200dpsY): return Qvals::GyroSampled200dps;
            case ((unsigned short int)RegisterAddresses::GyroSampledMinus200dpsZ): return Qvals::GyroSampled200dps;
            case ((unsigned short int)RegisterAddresses::GyroBiasAt25degCX): return Qvals::GyroBiasAt25degC;
            case ((unsigned short int)RegisterAddresses::GyroBiasAt25degCY): return Qvals::GyroBiasAt25degC;
            case ((unsigned short int)RegisterAddresses::GyroBiasAt25degCZ): return Qvals::GyroBiasAt25degC;
            case ((unsigned short int)RegisterAddresses::GyroBiasTempSensitivityX): return Qvals::GyroBiasTempSensitivity;
            case ((unsigned short int)RegisterAddresses::GyroBiasTempSensitivityY): return Qvals::GyroBiasTempSensitivity;
            case ((unsigned short int)RegisterAddresses::GyroBiasTempSensitivityZ): return Qvals::GyroBiasTempSensitivity;
            case ((unsigned short int)RegisterAddresses::GyroSample1Temp): return Qvals::CalibratedTherm;
            case ((unsigned short int)RegisterAddresses::GyroSample1BiasX): return Qvals::GyroSampledBias;
            case ((unsigned short int)RegisterAddresses::GyroSample1BiasY): return Qvals::GyroSampledBias;
            case ((unsigned short int)RegisterAddresses::GyroSample1BiasZ): return Qvals::GyroSampledBias;
            case ((unsigned short int)RegisterAddresses::GyroSample2Temp): return Qvals::CalibratedTherm;
            case ((unsigned short int)RegisterAddresses::GyroSample2BiasX): return Qvals::GyroSampledBias;
            case ((unsigned short int)RegisterAddresses::GyroSample2BiasY): return Qvals::GyroSampledBias;
            case ((unsigned short int)RegisterAddresses::GyroSample2BiasZ): return Qvals::GyroSampledBias;
            case ((unsigned short int)RegisterAddresses::AccelSensitivityX): return Qvals::AccelSensitivity;
            case ((unsigned short int)RegisterAddresses::AccelSensitivityY): return Qvals::AccelSensitivity;
            case ((unsigned short int)RegisterAddresses::AccelSensitivityZ): return Qvals::AccelSensitivity;
            case ((unsigned short int)RegisterAddresses::AccelBiasX): return Qvals::AccelBias;
            case ((unsigned short int)RegisterAddresses::AccelBiasY): return Qvals::AccelBias;
            case ((unsigned short int)RegisterAddresses::AccelBiasZ): return Qvals::AccelBias;
            case ((unsigned short int)RegisterAddresses::AccelSampledPlus1gX): return Qvals::AccelSampled1g;
            case ((unsigned short int)RegisterAddresses::AccelSampledPlus1gY): return Qvals::AccelSampled1g;
            case ((unsigned short int)RegisterAddresses::AccelSampledPlus1gZ): return Qvals::AccelSampled1g;
            case ((unsigned short int)RegisterAddresses::AccelSampledMinus1gX): return Qvals::AccelSampled1g;
            case ((unsigned short int)RegisterAddresses::AccelSampledMinus1gY): return Qvals::AccelSampled1g;
            case ((unsigned short int)RegisterAddresses::AccelSampledMinus1gZ): return Qvals::AccelSampled1g;
            case ((unsigned short int)RegisterAddresses::MagSensitivityX): return Qvals::MagSensitivity;
            case ((unsigned short int)RegisterAddresses::MagSensitivityY): return Qvals::MagSensitivity;
            case ((unsigned short int)RegisterAddresses::MagSensitivityZ): return Qvals::MagSensitivity;
            case ((unsigned short int)RegisterAddresses::MagBiasX): return Qvals::MagBias;
            case ((unsigned short int)RegisterAddresses::MagBiasY): return Qvals::MagBias;
            case ((unsigned short int)RegisterAddresses::MagBiasZ): return Qvals::MagBias;
            case ((unsigned short int)RegisterAddresses::MagHardIronBiasX): return Qvals::MagHardIronBias;
            case ((unsigned short int)RegisterAddresses::MagHardIronBiasY): return Qvals::MagHardIronBias;
            case ((unsigned short int)RegisterAddresses::MagHardIronBiasZ): return Qvals::MagHardIronBias;
            case ((unsigned short int)RegisterAddresses::AlgorithmKp): return Qvals::AlgorithmKp;
            case ((unsigned short int)RegisterAddresses::AlgorithmKi): return Qvals::AlgorithmKi;
            case ((unsigned short int)RegisterAddresses::AlgorithmInitKp): return Qvals::AlgorithmInitKp;
            case ((unsigned short int)RegisterAddresses::AlgorithmInitPeriod): return Qvals::AlgorithmInitPeriod;
            case ((unsigned short int)RegisterAddresses::AlgorithmMinValidMag): return Qvals::CalibratedMag;
            case ((unsigned short int)RegisterAddresses::AlgorithmMaxValidMag): return Qvals::CalibratedMag;
            case ((unsigned short int)RegisterAddresses::AlgorithmTareQuat0): return Qvals::Quaternion;
            case ((unsigned short int)RegisterAddresses::AlgorithmTareQuat1): return Qvals::Quaternion;
            case ((unsigned short int)RegisterAddresses::AlgorithmTareQuat2): return Qvals::Quaternion;
            case ((unsigned short int)RegisterAddresses::AlgorithmTareQuat3): return Qvals::Quaternion;
            case ((unsigned short int)RegisterAddresses::BattShutdownVoltage): return Qvals::CalibratedBatt;
            case ((unsigned short int)RegisterAddresses::AnalogueInputSensitivity): return Qvals::AnalogueInputSensitivity;
            case ((unsigned short int)RegisterAddresses::AnalogueInputBias): return Qvals::AnalogueInputBias;
            case ((unsigned short int)RegisterAddresses::ADXL345AsensitivityX): return Qvals::ADXL345busSensitivity;
            case ((unsigned short int)RegisterAddresses::ADXL345AsensitivityY): return Qvals::ADXL345busSensitivity;
            case ((unsigned short int)RegisterAddresses::ADXL345AsensitivityZ): return Qvals::ADXL345busSensitivity;
            case ((unsigned short int)RegisterAddresses::ADXL345AbiasX): return Qvals::ADXL345busBias;
            case ((unsigned short int)RegisterAddresses::ADXL345AbiasY): return Qvals::ADXL345busBias;
            case ((unsigned short int)RegisterAddresses::ADXL345AbiasZ): return Qvals::ADXL345busBias;
            case ((unsigned short int)RegisterAddresses::ADXL345BsensitivityX): return Qvals::ADXL345busSensitivity;
            case ((unsigned short int)RegisterAddresses::ADXL345BsensitivityY): return Qvals::ADXL345busSensitivity;
            case ((unsigned short int)RegisterAddresses::ADXL345BsensitivityZ): return Qvals::ADXL345busSensitivity;
            case ((unsigned short int)RegisterAddresses::ADXL345BbiasX): return Qvals::ADXL345busBias;
            case ((unsigned short int)RegisterAddresses::ADXL345BbiasY): return Qvals::ADXL345busBias;
            case ((unsigned short int)RegisterAddresses::ADXL345BbiasZ): return Qvals::ADXL345busBias;
            case ((unsigned short int)RegisterAddresses::ADXL345CsensitivityX): return Qvals::ADXL345busSensitivity;
            case ((unsigned short int)RegisterAddresses::ADXL345CsensitivityY): return Qvals::ADXL345busSensitivity;
            case ((unsigned short int)RegisterAddresses::ADXL345CsensitivityZ): return Qvals::ADXL345busSensitivity;
            case ((unsigned short int)RegisterAddresses::ADXL345CbiasX): return Qvals::ADXL345busBias;
            case ((unsigned short int)RegisterAddresses::ADXL345CbiasY): return Qvals::ADXL345busBias;
            case ((unsigned short int)RegisterAddresses::ADXL345CbiasZ): return Qvals::ADXL345busBias;
            case ((unsigned short int)RegisterAddresses::ADXL345DsensitivityX): return Qvals::ADXL345busSensitivity;
            case ((unsigned short int)RegisterAddresses::ADXL345DsensitivityY): return Qvals::ADXL345busSensitivity;
            case ((unsigned short int)RegisterAddresses::ADXL345DsensitivityZ): return Qvals::ADXL345busSensitivity;
            case ((unsigned short int)RegisterAddresses::ADXL345DbiasX): return Qvals::ADXL345busBias;
            case ((unsigned short int)RegisterAddresses::ADXL345DbiasY): return Qvals::ADXL345busBias;
            case ((unsigned short int)RegisterAddresses::ADXL345DbiasZ): return Qvals::ADXL345busBias;
            default: std::cout << "Register address does not have associated Qval:" << address << "\n"; return Qvals::CalibratedBatt; break;
        }
    }
    

private: 
    unsigned short int privAddress;
    unsigned short int privValue;
};

#endif //H_REGISTERDATA
