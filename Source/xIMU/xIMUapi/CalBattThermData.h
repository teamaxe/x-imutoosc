﻿#ifndef H_CALBATTTHERMALDATA
#define H_CALBATTTHERMALDATA

#include "xIMUdata.h"


class CalBattThermData : public xIMUdata
{	
	/**	
	Initialises a new instance of the <see cref="CalBattThermData"/> class.
	*/
public: 
	CalBattThermData() : privBatteryVoltage(0), privThermometer(0)
	{

	}
	
	/**
	Initialises a new instance of the <see cref="CalBattThermData"/> class.
	
	<param name="batteryVoltage">
	Calibrated battery voltage data in volts.
	</param>
	<param name="thermometer">
	Calibrated thermometer data degrees Celsius.
	*/
	
	CalBattThermData(float batteryVoltage, float thermometer)
	{
		privBatteryVoltage = batteryVoltage;
		privThermometer = thermometer;
	}
	
	/**
	/// Converts data to string of Comma Separated Variables.
	*/
//	string ConvertToCSV()
//	{
//		return Convert.ToString(BatteryVoltage) + "," + Convert.ToString(Thermometer);
//	}
	
	
	void setBatteryVoltage(float newBatteryVoltage){privBatteryVoltage = newBatteryVoltage;}
	float getBatteryVoltage(){return privBatteryVoltage;}
	
	void setThermometer(float newThermometer){privThermometer = newThermometer;}
	float getThermometer(){return privThermometer;}
	
private:
	float privBatteryVoltage;
	float privThermometer;
    
};

#endif //