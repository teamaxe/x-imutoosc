﻿#ifndef H_ERRORDATA
#define H_ERRORDATA

#include <string>

#include "xIMUdata.h"

//namespace xIMU_API
//{
    /**
    /// Error data class.
    */
    class ErrorData : public xIMUdata
    {
	public:
		/**
		 /// Initialises a new instance of the <see cref="ErrorData"/> class.
		 /// </summary>
		 */
        ErrorData()
        {
			setErrorCode(ErrorCodes::NoError);
        }
		
        /**
		 /// Initialises a new instance of the ErrorData class.
		 /// </summary>
		 /// <param name="errorCode">
		 /// Error code. Must be defined in <see cref="ErrorCodes"/>.
		 /// </param>
		 */
		ErrorData(const unsigned short initErrorCode)
        {
            setErrorCode(initErrorCode);
        }
		
		
		
		/**
        /// Error code. Must be defined in <see cref="ErrorCodes"/>.
        /// </summary>
        /// <exception cref="System.Exception">
        /// Thrown if invalid error code specifed.
        /// </exception>
		 */
		unsigned short getErrorCode()
		{
			return errorCode;
		}
		
		void setErrorCode(const unsigned short newErrorCode)
		{
			if (newErrorCode < ErrorCodes::NumErrorCodes) {
				errorCode = newErrorCode;
			}
			else 
			{
				std::cout << "Invalid error code.\n";
			}

		}
		
       
		
		
        /**
        /// Message associated with error code.
        /// </summary>
		 */
		std::string GetMessage()
        {
			std::string message = "Unknown error.";
            switch (errorCode)
            {
                case ((int)ErrorCodes::NoError): message = "No error."; break;
                case ((int)ErrorCodes::LowBattery): message = "Low battery."; break;
                case ((int)ErrorCodes::USBreceiveBufferOverrun): message = "USB receive buffer overrun."; break;
                case ((int)ErrorCodes::USBtransmitBufferOverrun): message = "USB transmit buffer overrun."; break;
                case ((int)ErrorCodes::BluetoothReceiveBufferOverrun): message = "Bluetooth receive buffer overrun."; break;
                case ((int)ErrorCodes::BluetoothTransmitBufferOverrun): message = "Bluetooth transmit buffer overrun."; break;
                case ((int)ErrorCodes::SDcardWriteBufferOverrun): message = "SD card write buffer overrun."; break;
                case ((int)ErrorCodes::TooFewBytesInPacket): message = "Too few bytes in packet."; break;
                case ((int)ErrorCodes::TooManyBytesInPacket): message = "Too many bytes in packet."; break;
                case ((int)ErrorCodes::InvalidChecksum): message = "Invalid checksum."; break;
                case ((int)ErrorCodes::UnknownHeader): message = "Unknown packet header."; break;
                case ((int)ErrorCodes::InvalidNumBytesForPacketHeader): message = "Invalid number of bytes for packet header."; break;
                case ((int)ErrorCodes::InvalidRegisterAddress): message = "Invalid register address."; break;
                case ((int)ErrorCodes::RegisterReadOnly): message = "Cannot write to read-only register."; break;
                case ((int)ErrorCodes::InvalidRegisterValue): message = "Invalid register value."; break;
                case ((int)ErrorCodes::InvalidCommand): message = "Invalid command."; break;
                case ((int)ErrorCodes::GyroscopeNotStationary): message = "Gyroscope not stationary. Calibration aborted."; break;
                case ((int)ErrorCodes::MagnetometerSaturation): message = "Magnetometer saturation occurred. Calibration aborted."; break;
                case ((int)ErrorCodes::IncorrectAuxillaryPortMode): message = "Auxiliary port in incorrect mode."; break;
                default: break;
            }
            return message;
        }
	private:
		unsigned short errorCode;

    };
//}
#endif // H_QUATERNIONDATA