/*
 *  RawAnalogueInputData.h
 *  Glover
 *
 *  Created by tjmitche on 19/12/2011.
 *  Copyright 2011 __MyCompanyName__. All rights reserved.
 *
 */

#ifndef H_RAWANALOGUEINPUTDATA
#define H_RAWANALOGUEINPUTDATA

#include "xIMUdata.h"

//namespace x_IMU_API
//{
/**
/// Raw analogue input data class.
*/
class RawAnalogueInputData : public xIMUdata
{
public:
    /**
     PortName enumeraiton
     */
    enum PortNames 
    {
        AnaloguePort0 = 0,
        AnaloguePort1,
        AnaloguePort2,
        AnaloguePort3,
        AnaloguePort4,
        AnaloguePort5,
        AnaloguePort6,
        AnaloguePort7,
        
        NumPorts
    };
    
    /**
     /// Initialises a new instance of the <see cref="RawAnalogueInputData"/> class.
     */
    RawAnalogueInputData()
    {
        initRawAnalogueInputData();
    }
    
    //Copy constructor
    RawAnalogueInputData(const RawAnalogueInputData& source)
    {
        initRawAnalogueInputData();
        std::vector<short> copyVector;
        source.getRawAnalogueInputData(&copyVector);
        setRawAnalogueInputData(copyVector);            
    }
    
    //Assingment overload
    RawAnalogueInputData& operator=(const RawAnalogueInputData& source)
    {
        if (this == &source)
            return *this;
        
        std::vector<short> copyVector;
        source.getRawAnalogueInputData(&copyVector);
        setRawAnalogueInputData(copyVector);
        
        return *this;            
    }
    
    
    /**
     /// Initialises a new instance of the <see cref="RawAnalogueInput"/> class.
     */
    /// <param name="AX0">
    /// Raw channel AX0 voltage ADC data in lsb.
    /// </param>
    /// <param name="AX1">
    /// Raw channel AX1 voltage ADC data in lsb.
    /// </param>
    /// <param name="AX2">
    /// Raw channel AX2 voltage ADC data in lsb.
    /// </param>
    /// <param name="AX3">
    /// Raw channel AX3 voltage ADC data in lsb.
    /// </param>
    /// <param name="AX4">
    /// Raw channel AX4 voltage ADC data in lsb.
    /// </param>
    /// <param name="AX5">
    /// Raw channel AX5 voltage ADC data in lsb.
    /// </param>
    /// <param name="AX6">
    /// Raw channel AX6 voltage ADC data in lsb.
    /// </param>
    /// <param name="AX7">
    /// Raw channel AX7 voltage ADC data in lsb.
    /// </param>
    RawAnalogueInputData(short AX0, short AX1, short AX2, short AX3, short AX4, short AX5, short AX6, short AX7)
    {
        initRawAnalogueInputData();
        rawAnalogueInputData[AnaloguePort0] = AX0;
        rawAnalogueInputData[AnaloguePort1] = AX1;
        rawAnalogueInputData[AnaloguePort2] = AX2;
        rawAnalogueInputData[AnaloguePort3] = AX3;
        rawAnalogueInputData[AnaloguePort4] = AX4;
        rawAnalogueInputData[AnaloguePort5] = AX5;
        rawAnalogueInputData[AnaloguePort6] = AX6;
        rawAnalogueInputData[AnaloguePort7] = AX7;
    }
    
    /**
     /// Initialises a new instance of the <see cref="RawAnalogueInputData"/> class.
     */
    /// <param name="analoguePort">
    /// Raw analogue port ADC data in lsb.  Elements 0 to 7 correspond to channels AX0 to AX7.
    /// </param>
    RawAnalogueInputData(std::vector<short> analogueInputData)
    {
        initRawAnalogueInputData();
        if (analogueInputData.size() == NumPorts) 
        {
            std::copy(analogueInputData.begin(), analogueInputData.end(), rawAnalogueInputData.begin());
        }
    }
    
    void initRawAnalogueInputData()
    {
        rawAnalogueInputData.resize(NumPorts, static_cast<short>(0));//Must be 4 elements init to zero
    }
    
    
    /**
    /// Raw analogue port ADC data in lsb.  Elements 0 to 7 correspond to channels AX0 to AX7.
    */
    void getRawAnalogueInputData(std::vector<short> *analogueInputCopyVector) const
    {
        analogueInputCopyVector->resize(NumPorts);
        std::copy(rawAnalogueInputData.begin(), rawAnalogueInputData.end(), analogueInputCopyVector->begin());
    }
    
    void setRawAnalogueInputData(const std::vector<short> &newAnalogueInput)
    {
        if (newAnalogueInput.size() == NumPorts)
        {
            std::copy(newAnalogueInput.begin(), newAnalogueInput.end(), rawAnalogueInputData.begin());
        }
    }
    
    /**
    /// Gets or sets raw channel AX0 voltage ADC data in lsb.
    */
    const short getPort(const int portNumber) const
    {
        return rawAnalogueInputData[portNumber];
    }
    
    void setPort(const int portNumber, const short newValue)
    {
        rawAnalogueInputData[portNumber] = newValue;
    }
    
    /**
    /// Converts data to string of Comma Separated Variables.
    */
    /// <returns>
    /// CSV line.
    /// </returns>
    void ConvertToCSV(std::string &analoguePortString) const
    {
        std::stringstream ss;
        
        ss << static_cast<int>(rawAnalogueInputData[0]) << "," 
        << static_cast<int>(rawAnalogueInputData[1]) << "," 
        << static_cast<int>(rawAnalogueInputData[2]) << "," 
        << static_cast<int>(rawAnalogueInputData[3]) << "," 
        << static_cast<int>(rawAnalogueInputData[4]) << "," 
        << static_cast<int>(rawAnalogueInputData[5]) << "," 
        << static_cast<int>(rawAnalogueInputData[6]) << "," 
        << static_cast<int>(rawAnalogueInputData[7]);
    
        analoguePortString = ss.str();
    }
    
private: 
    std::vector <short> rawAnalogueInputData;
    
    
};
//}

#endif //H_RAWANALOGUEINPUTDATA

