﻿#ifndef H_PACKETCONSTRUCTION
#define H_PACKETCONSTRUCTION

#include <iostream>
#include <vector>

#include "PacketEncoding.h"
#include "DigitalIOdata.h"
#include "Enums.h"
#include "FixedFloat.h"
#include "ErrorData.h"
#include "CalBattThermData.h"
#include "QuaternionData.h"
#include "CalInertialMagData.h"
#include "RawAnalogueInputData.h"
#include "PWMoutputData.h"
#include "RawBattThermData.h"
#include "DateTimeData.h"
#include "CommandData.h"
#include "RegisterData.h"

//TM:returning dynamically allocated objects is a bit gross!

/**
 /// Packet construction class. Contains static methods for packet construction and deconstruction.
 */
class PacketConstruction
{
public:	
	/**
	 /// Constructs an encoded command packet.
	 /// <param name="commandCode">
	 /// Command code. See <see cref="CommandCodes"/>.
	 /// </param> 
	 /// <returns>
	 /// Constructed and encoded command packet.
	 /// </returns> 
	 */
	static void ConstructCommandPacket(std::vector<unsigned char> &encodedPacketVec, const CommandCodes::CommandCodes_ commandCode)
	{
		const unsigned int decodedPacketSize = 4;
		unsigned char decodedPacket[decodedPacketSize] = {	(unsigned char)PacketHeaders::Command, 
            (unsigned char)(commandCode >> 8), 
            (unsigned char)commandCode, 
            0}; 
		
		InsertChecksum(decodedPacket, decodedPacketSize);
		
		unsigned char encodedPacket[25];
		const unsigned int encodedPacketSize = PacketEncoding::EncodePacket(encodedPacket, decodedPacket, decodedPacketSize);
        encodedPacketVec.resize(encodedPacketSize);
		copy(encodedPacket, encodedPacket+encodedPacketSize, encodedPacketVec.begin());
	}
	
	/**
	 /// Constructs a read register packet.
	 */
	/// <param name="registerData">
	/// <see cref="RegisterData"/> instance containing register address to be read.
	/// </param>
	/// <returns>
	/// Constructed and encoded read register packet
	/// </returns> 
    //	public static byte[] ConstructReadRegisterPacket(RegisterData registerData)
    //	{
    //		byte[] decodedPacket = new byte[] { (byte)PacketHeaders.ReadRegister,
    //			(byte)(registerData.Address >> 8),
    //			(byte)registerData.Address,
    //			0};
    //		decodedPacket = InsertChecksum(decodedPacket);
    //		return PacketEncoding.EncodePacket(decodedPacket);
    //	}
	
	/**
	 /// Constructs a write register packet.
	 */
	/// <param name="registerData">
	/// <see cref="RegisterData"/> instance containing register address and value to be written.
	/// </param>
	/// <returns>
	/// Constructed and encoded write register packet
	/// </returns>
    
    static int ConstructWriteRegisterPacket(std::vector<unsigned char> &encodedPacketVec, RegisterData registerData)
    {
        const unsigned int decodedPacketSize = 6;
        
        unsigned char decodedPacket[decodedPacketSize] = { (unsigned char)PacketHeaders::WriteRegister,
            (unsigned char)(registerData.getAddress() >> 8),
            (unsigned char)registerData.getAddress(),
            (unsigned char)(registerData.getValue() >> 8),
            (unsigned char)registerData.getValue(),
            0};
        
        InsertChecksum(decodedPacket, decodedPacketSize);
        
        unsigned char encodedPacket[256];
        const unsigned int encodedPacketSize = PacketEncoding::EncodePacket(encodedPacket, decodedPacket, decodedPacketSize);
        
    	encodedPacketVec.resize(encodedPacketSize);
		copy(encodedPacket, encodedPacket+encodedPacketSize, encodedPacketVec.begin());
        
        return encodedPacketSize;
    }
	
	/**
	 /// Constructs a read date/time packet.
	 */
	/// <returns>
	/// Constructed and encoded read date/time packet
	/// </returns> 
    //	public static byte[] ConstructReadDateTimePacket()
    //	{
    //		byte[] decodedPacket = new byte[] { (byte)PacketHeaders.ReadDateTime,
    //			0};
    //		decodedPacket = InsertChecksum(decodedPacket);
    //		return PacketEncoding.EncodePacket(decodedPacket);
    //	}
	
	/**
	 /// Constructs a write date/time packet.
	 */
	/// <param name="dateTimeData">
	/// <see cref="DateTimeData"/> instance containing data to be written.
	/// </param>
	/// <returns>
	/// Constructed and encoded write date/time packet
	/// </returns> 
    //	public static byte[] ConstructWriteDateTimePacket(DateTimeData dateTimeData)
    //	{
    //		byte[] decodedPacket = new byte[] { (byte)PacketHeaders.WriteDateTime,
    //			(byte)(((((dateTimeData.Year - 2000) % 100) / 10) << 4) | ((dateTimeData.Year - 2000) % 10)),
    //			(byte)(((((dateTimeData.Month) % 100) / 10) << 4) | (dateTimeData.Month % 10)),
    //			(byte)(((((dateTimeData.Date) % 100) / 10) << 4) | (dateTimeData.Date % 10)),
    //			(byte)(((((dateTimeData.Hours) % 100) / 10) << 4) | (dateTimeData.Hours % 10)),
    //			(byte)(((((dateTimeData.Minutes) % 100) / 10) << 4) | (dateTimeData.Minutes % 10)),
    //			(byte)(((((dateTimeData.Seconds) % 100) / 10) << 4) | (dateTimeData.Seconds % 10)),
    //			0};
    //		decodedPacket = InsertChecksum(decodedPacket);
    //		return PacketEncoding.EncodePacket(decodedPacket);
    //	}
	
	/**
	 /// Constructs an encoded digital I/O packet.
	 */
	/// <param name="digitalIOData">
	/// <see cref="DigitalIOData"/> instance containing output states to be set.
	/// </param> 
	/// <returns>
	/// Constructed and encoded digital I/O packet.
	/// </returns> 
	
	static int ConstructDigitalIOPacket(std::vector<unsigned char> &encodedPacketVec,  const DigitalIOdata &digitalIOData)
	{
		const unsigned int decodedPacketSize = 4;
		unsigned char decodedPacket[decodedPacketSize] = { (unsigned char)PacketHeaders::DigitalIOdata,
            0x00,
            digitalIOData.getState().getPortBitsAsUnsignedChar(),
            0};
		
		InsertChecksum(decodedPacket, decodedPacketSize);
		
		unsigned char encodedPacket[256];
		const unsigned int encodedPacketSize = PacketEncoding::EncodePacket(encodedPacket, decodedPacket, decodedPacketSize);
		
		encodedPacketVec.resize(encodedPacketSize);
		copy(encodedPacket, encodedPacket+encodedPacketSize, encodedPacketVec.begin());
        
        return encodedPacketSize;
	}
    
    /**
     /// Constructs an encoded PWM output data packet.
     /// </summary>
     /// <param name="_PWMoutputData">
     /// PWM output data.
     /// </param>
     /// <returns>
     /// Constructed and encoded PWM output data packet.
     */
    static int ConstructPWMoutputPacket(std::vector<unsigned char> &encodedPacketVec,  const PWMoutputData &pwmOutputData)
    {
        short unsigned int AX0short = FixedFloat::FloatToFixed(pwmOutputData.getAX0(), Qvals::PWMoutput);
        short unsigned int AX2short = FixedFloat::FloatToFixed(pwmOutputData.getAX2(), Qvals::PWMoutput);
        short unsigned int AX4short = FixedFloat::FloatToFixed(pwmOutputData.getAX4(), Qvals::PWMoutput);
        short unsigned int AX6short = FixedFloat::FloatToFixed(pwmOutputData.getAX6(), Qvals::PWMoutput);
		
		AX0short *= 2;
		AX2short *= 2;
		AX4short *= 2;
        AX6short *= 2;
		
        //std::cout << AX0short << ":" << AX2short << ":" << AX4short << ":" << AX6short << "\n";
        
        const unsigned int decodedPacketSize = 10;
		unsigned char decodedPacket[decodedPacketSize] = 
        { 
            (unsigned char)PacketHeaders::PWMoutputData,
            (unsigned char)(AX0short >> 8),
            (unsigned char)AX0short,
            (unsigned char)(AX2short >> 8),
            (unsigned char)AX2short,
            (unsigned char)(AX4short >> 8),
            (unsigned char)AX4short,
            (unsigned char)(AX6short >> 8),
            (unsigned char)AX6short,
            0};
        
        InsertChecksum(decodedPacket, decodedPacketSize);
        
        unsigned char encodedPacket[256];//more than enough
		const unsigned int encodedPacketSize = PacketEncoding::EncodePacket(encodedPacket, decodedPacket, decodedPacketSize);
        
        encodedPacketVec.resize(encodedPacketSize);
		copy(encodedPacket, encodedPacket+encodedPacketSize, encodedPacketVec.begin());
        
        return encodedPacketSize;
    }
	
	/**
	 /// Inserts check sum at last position in array equal to the sum all of bytes preceding it.
	 */
	/// <param name="decodedPacket">
	/// Decoded packet byte array.
	/// </param>
	/// <returns>
	/// Decoded packet with checksum inserted at last position in array.
	/// </returns> 
	static void InsertChecksum(unsigned char *packet, const unsigned int packetSize)
	{
		packet[packetSize - 1] = 0;                        // zero current checksum
		for (int i = 0; i < (packetSize - 1); i++)
		{
			packet[packetSize - 1] += packet[i];    // acclimate checksum
		}
	}
    
	
	/**
	 /// Deconstructs packet from and encoded byte array and return data object.
	 */
	/// <param name="encodedPacket">
	/// Byte array containing the encoded packet.
	/// </param>
	/// <returns>
	/// <see cref="xIMUdata"/> object deconstructed from packet.
	/// </returns>  
	/// <exception cref="System.Exception">
	/// Thrown when deconstruction of an invalid packet is attempted.
	/// </exception>
	static xIMUdata *DeconstructPacket(const unsigned char* const encodedPacket, unsigned int encodedSize)
	{
		// Decode packet
		if (encodedSize < 4)
		{
			//throw new Exception("Too few bytes in packet.");
			std::cerr << "Too few bytes in packet:" << encodedSize << "\n";
			return NULL;
		}
		else if (encodedSize > 23)
		{
			//throw new Exception("Too many bytes in packet.");
			std::cerr << "Too many bytes in packet.\n";
			return NULL;
		}
		unsigned char decodedPacket[24];
		unsigned int decodedSize = PacketEncoding::DecodePacket(encodedPacket, decodedPacket, encodedSize);
		
		// Confirm checksum
		unsigned char checksum = 0;
		for (int i = 0; i < (decodedSize - 1); i++) checksum += decodedPacket[i];
		if (checksum != decodedPacket[decodedSize - 1])
		{
			//throw new Exception("Invalid checksum.");
			std::cerr << "Invalid checksum.";
			return NULL;
		}
		
		// Interpret packet according to header
        //std::cout << "PacketType:~" << decodedPacket[0] << "~";
		switch (decodedPacket[0])
		{
                //case ((byte)PacketHeaders.ErrorMessage):
			case (PacketHeaders::ErrorMessage):
				if (decodedSize != 4)
				{
					//throw new Exception("Invalid number of bytes for packet header.");
					std::cerr << "Invalid number of bytes for packet header: decodedSize = " << decodedSize << "\n";
					return NULL;
				}
				std::cerr << "This is an Error Message = " << decodedSize << "\n";
				return new ErrorData((unsigned short)Combine(decodedPacket[1], decodedPacket[2]));
                
			case ((unsigned char)PacketHeaders::Command):
				if (decodedSize != 4)
				{
					//throw new Exception("Invalid number of bytes for packet header.");
					std::cerr << "Invalid number of bytes for packet header: decodedSize = " << decodedSize << "\n";
					return NULL;
				}
				//printf("deconstruct: %d %d %d %d\n", decodedPacket[0] , decodedPacket[1], decodedPacket[2], decodedPacket[3]);
				//std::cerr << "*************************************************command data\n";
				//return NULL;
                return new CommandData((unsigned short)Combine(decodedPacket[1], decodedPacket[2]));
            case ((unsigned char)PacketHeaders::WriteRegister):
                if (decodedSize != 6)
                {
                    std::cerr << "Invalid number of bytes for packet header: decodedSize = " << decodedSize << "\n";
                }
                return new RegisterData((unsigned short int)Combine(decodedPacket[1], decodedPacket[2]), (unsigned short int)Combine(decodedPacket[3], decodedPacket[4]));
            case (PacketHeaders::WriteDateTime):
                if (decodedSize != 8)
                {
                    //	throw new Exception("Invalid number of bytes for packet header.");
                }
                return new DateTimeData((int)(10 * ((decodedPacket[1] & 0xF0) >> 4) + (decodedPacket[1] & 0x0F)) + 2000,
                                        (int)(10 * ((decodedPacket[2] & 0xF0) >> 4) + (decodedPacket[2] & 0x0F)),
                                        (int)(10 * ((decodedPacket[3] & 0xF0) >> 4) + (decodedPacket[3] & 0x0F)),
                                        (int)(10 * ((decodedPacket[4] & 0xF0) >> 4) + (decodedPacket[4] & 0x0F)),
                                        (int)(10 * ((decodedPacket[5] & 0xF0) >> 4) + (decodedPacket[5] & 0x0F)),
                                        (int)(10 * ((decodedPacket[6] & 0xF0) >> 4) + (decodedPacket[6] & 0x0F)));
            case (PacketHeaders::RawBattThermData):
                if (decodedSize != 6)
                {
                    std::cerr << "Invalid number of bytes for packet header. DecodedSize=" << decodedSize << "\n";
                }
                return new RawBattThermData(Combine(decodedPacket[1], decodedPacket[2]), Combine(decodedPacket[3], decodedPacket[4]));
                
			case (PacketHeaders::CalBattThermData):
				if (decodedSize != 6)
				{
					//throw new Exception("Invalid number of bytes for packet header.");
					std::cerr << "Invalid number of bytes for packet header. DecodedSize=" << decodedSize << "\n";
					return NULL;
				}
				return new CalBattThermData(FixedFloat::FixedToFloat(Combine(decodedPacket[1], decodedPacket[2]), Qvals::CalibratedBatt),
											FixedFloat::FixedToFloat(Combine(decodedPacket[3], decodedPacket[4]), Qvals::CalibratedTherm));
				
                //			case ((byte)PacketHeaders.RawInertialMagData):
                //				if (size != 20)
                //				{
                //					throw new Exception("Invalid number of bytes for packet header.");
                //				}
                //				return new RawInertialMagData(new short[] { Combine(decodedPacket[1], decodedPacket[2]), Combine(decodedPacket[3], decodedPacket[4]), Combine(decodedPacket[5], decodedPacket[6]) },
                //											  new short[] { Combine(decodedPacket[7], decodedPacket[8]), Combine(decodedPacket[9], decodedPacket[10]), Combine(decodedPacket[11], decodedPacket[12]) },
                //											  new short[] { Combine(decodedPacket[13], decodedPacket[14]), Combine(decodedPacket[15], decodedPacket[16]), Combine(decodedPacket[17], decodedPacket[18]) });
                
			case (PacketHeaders::CalInertialMagneticData):
				if (decodedSize != 20)
				{
					//throw new Exception("Invalid number of bytes for packet header.");
					std::cerr << "Invalid number of bytes for packet header. DecodedSize=" << decodedSize << "\n";
					return NULL;
				}
				else 
				{
                    std::vector<float> gyroscope;
					gyroscope.push_back(FixedFloat::FixedToFloat(Combine(decodedPacket[1], decodedPacket[2]), Qvals::CalibratedGyro));
					gyroscope.push_back(FixedFloat::FixedToFloat(Combine(decodedPacket[3], decodedPacket[4]), Qvals::CalibratedGyro));
					gyroscope.push_back(FixedFloat::FixedToFloat(Combine(decodedPacket[5], decodedPacket[6]), Qvals::CalibratedGyro));
                    
                    std::vector<float> accelerometer;
					accelerometer.push_back(FixedFloat::FixedToFloat(Combine(decodedPacket[7], decodedPacket[8]), Qvals::CalibratedAccel));
					accelerometer.push_back(FixedFloat::FixedToFloat(Combine(decodedPacket[9], decodedPacket[10]), Qvals::CalibratedAccel));
					accelerometer.push_back(FixedFloat::FixedToFloat(Combine(decodedPacket[11], decodedPacket[12]), Qvals::CalibratedAccel));
                    
                    std::vector<float> magnetometer;
					magnetometer.push_back(FixedFloat::FixedToFloat(Combine(decodedPacket[13], decodedPacket[14]), Qvals::CalibratedMag));
					magnetometer.push_back(FixedFloat::FixedToFloat(Combine(decodedPacket[15], decodedPacket[16]), Qvals::CalibratedMag));
					magnetometer.push_back(FixedFloat::FixedToFloat(Combine(decodedPacket[17], decodedPacket[18]), Qvals::CalibratedMag));
                    
                    return new CalInertialMagData(gyroscope, accelerometer, magnetometer);
				}
				
			case (PacketHeaders::QuaternionData):
				if (decodedSize != 10)
				{
					//throw new Exception("Invalid number of bytes for packet header.");
					std::cout << "Invalid number of bytes for packet header. DecodedSize=" << decodedSize << "\n";
					return NULL;
				}
				else 
				{
					std::vector<float> quaternionVector;
					quaternionVector.push_back(FixedFloat::FixedToFloat(Combine(decodedPacket[1], decodedPacket[2]), Qvals::Quaternion));
					quaternionVector.push_back(FixedFloat::FixedToFloat(Combine(decodedPacket[3], decodedPacket[4]), Qvals::Quaternion));
					quaternionVector.push_back(FixedFloat::FixedToFloat(Combine(decodedPacket[5], decodedPacket[6]), Qvals::Quaternion));
					quaternionVector.push_back(FixedFloat::FixedToFloat(Combine(decodedPacket[7], decodedPacket[8]), Qvals::Quaternion));
                    
					return new QuaternionData(quaternionVector);
				}
				
                //				return new QuaternionData(new float[] { FixedFloat.FixedToFloat(Combine(decodedPacket[1], decodedPacket[2]), Qvals.Quaternion), FixedFloat.FixedToFloat(Combine(decodedPacket[3], decodedPacket[4]), Qvals.Quaternion),
                //					FixedFloat.FixedToFloat(Combine(decodedPacket[5], decodedPacket[6]), Qvals.Quaternion), FixedFloat.FixedToFloat(Combine(decodedPacket[7], decodedPacket[8]), Qvals.Quaternion)});
			case(PacketHeaders::DigitalIOdata):
				if (decodedSize != 4)
				{
					//throw new Exception("Invalid number of bytes for packet header.");
					std::cout << "Invalid number of bytes for packet header. DecodedSize=" << decodedSize << "\n";
					return NULL;
				}
				return new DigitalIOdata(DigitalPortBits(decodedPacket[1]), DigitalPortBits(decodedPacket[2]));
                
            case (PacketHeaders::RawAnalogueInputData):
                if (decodedSize != 18)
                {
                    //throw new Exception("Invalid number of bytes for packet header.");
					std::cout << "Invalid number of bytes for packet header. DecodedSize=" << decodedSize << "\n";
                }
                return new RawAnalogueInputData(Combine(decodedPacket[1], decodedPacket[2]), Combine(decodedPacket[3], decodedPacket[4]), Combine(decodedPacket[5], decodedPacket[6]), Combine(decodedPacket[7], decodedPacket[8]),
                                                Combine(decodedPacket[9], decodedPacket[10]), Combine(decodedPacket[11], decodedPacket[12]), Combine(decodedPacket[13], decodedPacket[14]), Combine(decodedPacket[15], decodedPacket[16]));
            case (PacketHeaders::PWMoutputData):
                if (decodedSize != 10)
                {
                    std::cout << "Invalid number of bytes for packet header. DecodedSize=" << decodedSize << "\n";
                }
                return new PWMoutputData(FixedFloat::FixedToFloat(Combine(decodedPacket[1], decodedPacket[2]), Qvals::PWMoutput), FixedFloat::FixedToFloat(Combine(decodedPacket[3], decodedPacket[4]), Qvals::PWMoutput),
                                         FixedFloat::FixedToFloat(Combine(decodedPacket[5], decodedPacket[6]), Qvals::PWMoutput), FixedFloat::FixedToFloat(Combine(decodedPacket[7], decodedPacket[8]), Qvals::PWMoutput));
				//return new DigitalIOdata(new DigitalIOdata.PortData(decodedPacket[1]), new DigitalIOdata.PortData(decodedPacket[2]));
			default:
				//throw new Exception("Unknown packet header.");
				std::cout << "**Unknown packet header. Packet Size:" << decodedSize;
				return NULL;
		}
	}
	
	/**
	 /// Combines a most significant and least significant byte to return a short.
	 */
	/// <param name="MSB">
	/// Most significant byte.
	/// </param>
	/// <param name="LSB">
	/// Least significant byte.
	/// </param> 
	/// <returns>
	/// MSB and LSB combined to create a short.
	/// </returns>
	//private 
	static short Combine(unsigned char MSB, unsigned char LSB)
	{
		return (short)((short)((short)MSB << 8) | (short)LSB);
	}
	
    //#endregion
};

#endif //H_PACKETCONSTRUCTION