﻿#ifndef H_RAWBATTTHERMDATA
#define H_RAWBATTTHERMDATA
//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;
//
//namespace x_IMU_API
//{
/**
 /// Raw battery voltage thermometer data class.
 */
class RawBattThermData : public xIMUdata
{
public:
    /**
     /// Gets or sets the raw battery voltage ADC data in LSBs.
     */
    
    short int getBatteryVoltage() {return privBatteryVoltage;}
    void setBatteryVoltage(short int newVoltage) {privBatteryVoltage = newVoltage;}
    
    /**
     /// Gets or sets the raw Thermometer ADC data in LSBs.
     */
    short int getTemperature () {return privTemperature;}
    void settemperature (short int newTemperature) {privTemperature = newTemperature;}
    
    /**
     /// Initialises a new instance of the <see cref="RawBattThermData"/> class.
     */
    RawBattThermData()
    {
        privBatteryVoltage = 0;
        privTemperature = 0;
        
    }
    
    /**
     /// Initialises a new instance of the <see cref="RawBattThermData"/> class.
     */
    /// <param name="batteryVoltage">
    /// Raw battery voltage ADC data in LSBs.
    /// </param>
    /// <param name="thermometer">
    /// Raw Thermometer ADC data in LSBs.
    /// </param>        
    RawBattThermData(short int batteryVoltage, short int temperature)
    {
        privBatteryVoltage = batteryVoltage;
        privTemperature = temperature;
    }
    
    /**
     /// Converts data to string of Comma Separated Variables.
     */
    /// <returns>
    /// CSV line.
    /// </returns>
    void ConvertToCSV(std::string &analoguePortString) const
    {
        std::stringstream ss;
        
        ss << privBatteryVoltage << "," << privTemperature;
        
        analoguePortString = ss.str();
    }
    
private:
    short int privBatteryVoltage;
    short int privTemperature;
};
#endif// H_RAWBATTTHERMDATA
