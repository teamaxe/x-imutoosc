﻿#ifndef H_QUATERNIONDATA
#define H_QUATERNIONDATA

#include <vector>
#include <algorithm>
#include <cmath>
#include <sstream>

#include "xIMUdata.h"

//namespace xIMU_API
//{
    /**
    /// Quaternion data class.
    /// </summary>
    /// <remarks>
    /// See http://www.x-io.co.uk/res/doc/quaternions.pdf for more information on quaternions.
    /// </remarks>
	 */
    class QuaternionData : public xIMUdata
    {
		
	public:
		/**
		 /// Initialises a new instance of the <see cref="QuaternionData"/> class.
		 /// </summary>
		 */
		QuaternionData()
		{
			initQuaternion();
		}
        
        //Copy constructor
        QuaternionData(const QuaternionData& source)
        {
            initQuaternion();
            std::vector<float> copyVector;
            source.getQuaternion(copyVector);
            setQuaternion(copyVector);            
        }
        
        //Assingment overload
        QuaternionData& operator=(const QuaternionData& source)
        {
            if (this == &source)
                return *this;
            
            std::vector<float> copyVector;
            source.getQuaternion(copyVector);
            setQuaternion(copyVector);
            
            return *this;            
        }

		
		/**
		 /// Initialises a new instance of the <see cref="QuaternionData"/> class.
		 /// </summary>
		 /// <param name="quaternion">
		 /// Quaternion. Must of 4 elements. Each element must be of value -1 to +1.
		 /// </param>
		 */
		QuaternionData(std::vector<float> quaternionData)
		{
			initQuaternion();
			if (quaternionData.size() == 4) 
			{
				std::copy(quaternionData.begin(), quaternionData.end(), quaternion.begin());
			}
		}
		
		void initQuaternion()
		{
			quaternion.resize(4, 0.f);//Must be 4 elements init to zero
			quaternion[0] = 1.f;
		}
		
		~QuaternionData()
		{
			
		}
		
		/**
		 /// Quaternion. Must of 4 elements. Each element must be of value -1 to +1.  Vector will be normalised.
		 /// </summary>
		 /// <exception cref="System.Exception">
		 /// Thrown if invalid quaternion specified.
		 /// </exception>
		 */
        
        const std::vector<float>& getQuaternion() const
        {
            return quaternion;
        }
        
		void getQuaternion(std::vector<float> &quaternionCopyVector) const
		{
			quaternionCopyVector.resize(4);
			std::copy(quaternion.begin(), quaternion.end(), quaternionCopyVector.begin());
		}
		
		void setQuaternion(const std::vector<float> &newQuaternion)
		{
			if (newQuaternion.size() == 4)
			{
				float norm = (float)std::sqrt(quaternion[0] * quaternion[0] + quaternion[1] * quaternion[1] + quaternion[2] * quaternion[2] + quaternion[3] * quaternion[3]);
				copy(newQuaternion.begin(), newQuaternion.end(), quaternion.begin());
				quaternion[0] /= norm;
				quaternion[1] /= norm;
				quaternion[2] /= norm;
				quaternion[3] /= norm;
			}
		}
		
		/**
		 /// Converts this quaternion to its conjugate.
		 /// </summary>
		 /// </returns>
		 */
        void ConvertToConjugate()
        {
			quaternion[1] = -quaternion[1];
			quaternion[2] = -quaternion[2];
			quaternion[3] = -quaternion[3];
        }
		
		/**
		 /// Converts from radians to degrees.
		 /// </summary>
		 /// <param name="radians">
		 /// Angular quantity in radians.
		 /// </param> 
		 /// <returns>
		 /// Angular quantity in degrees.
		 /// </returns>
		 */
        float Rad2Deg(float radians) const
        {
            return 57.2957795130823f * radians;
        }
		
		/**
		 /// Converts data to rotation matrix.
		 /// </summary>
		 /// <remarks>
		 /// Elements 0 to 2 represent columns 1 to 3 of row 1.
		 /// Elements 3 to 5 represent columns 1 to 3 of row 2.
		 /// Elements 6 to 8 represent columns 1 to 3 of row 3.
		 /// </remarks>
		 */
        void ConvertToRotationMatrix(std::vector <float> &rotMatrixVector) const
        {
            float R11 = 2 * quaternion[0] * quaternion[0] - 1 + 2 * quaternion[1] * quaternion[1];
            float R12 = 2 * (quaternion[1] * quaternion[2] + quaternion[0] * quaternion[3]);
            float R13 = 2 * (quaternion[1] * quaternion[3] - quaternion[0] * quaternion[2]);
            float R21 = 2 * (quaternion[1] * quaternion[2] - quaternion[0] * quaternion[3]);
            float R22 = 2 * quaternion[0] * quaternion[0] - 1 + 2 * quaternion[2] * quaternion[2];
            float R23 = 2 * (quaternion[2] * quaternion[3] + quaternion[0] * quaternion[1]);
            float R31 = 2 * (quaternion[1] * quaternion[3] + quaternion[0] * quaternion[2]);
            float R32 = 2 * (quaternion[2] * quaternion[3] - quaternion[0] * quaternion[1]);
            float R33 = 2 * quaternion[0] * quaternion[0] - 1 + 2 * quaternion[3] * quaternion[3];
			float rotMatrixArray[9] =	{	R11, R12, R13,
											R21, R22, R23,
											R31, R32, R33 };
			rotMatrixVector.resize(9);
			std::copy(rotMatrixArray, rotMatrixArray+9, rotMatrixVector.begin());
        }
		
		/**
		 /// Converts data to XYZ Euler angles (in degrees).
		 /// </summary>
		 */
        void ConvertToEulerAngles(std::vector <float> &eulerAnglesVector) const
        {
            float phi = (float)std::atan2(2 * (quaternion[2] * quaternion[3] - quaternion[0] * quaternion[1]), 2 * quaternion[0] * quaternion[0] - 1 + 2 * quaternion[3] * quaternion[3]);
            float theta = (float)-std::atan((2.0 * (quaternion[1] * quaternion[3] + quaternion[0] * quaternion[2])) / std::sqrt(1.0 - std::pow((2.0 * quaternion[1] * quaternion[3] + 2.0 * quaternion[0] * quaternion[2]), 2.0)));
            float psi = (float)std::atan2(2 * (quaternion[1] * quaternion[2] - quaternion[0] * quaternion[3]), 2 * quaternion[0] * quaternion[0] - 1 + 2 * quaternion[1] * quaternion[1]);
            float eulerAnglesArray[3] = { Rad2Deg(phi), Rad2Deg(theta), Rad2Deg(psi) };
			
			eulerAnglesVector.resize(3);
			std::copy(eulerAnglesArray, eulerAnglesArray+3, eulerAnglesVector.begin());
        }
		
		/**
		 /// Converts data to string of Comma Separated Variables representaing the elements of a rotation matrix.
		 /// </summary>
		 /// <remarks>
		 /// First to third values represent columns 1 to 3 of row 1.
		 /// Forth to fifth values represent columns 1 to 3 of row 2.
		 /// Sixth to eighth values represent columns 1 to 3 of row 3.
		 /// </remarks>
		 */ 
		void ConvertToRotationMatrixCSV(std::string &rotMatrixString) const
        {
			std::vector<float> rotMatrixVector;
			ConvertToRotationMatrix(rotMatrixVector);
			
			std::stringstream ss;
            ss << rotMatrixVector[0] << "," << rotMatrixVector[1] << "," << rotMatrixVector[2] << "," <<
			rotMatrixVector[3] << "," << rotMatrixVector[4] << "," << rotMatrixVector[5] << "," <<
			rotMatrixVector[6] << "," << rotMatrixVector[7] << "," << rotMatrixVector[8];
			
			rotMatrixString = ss.str();
        }
		
        /**
		 /// Converts data to string of Comma Separated Variables representaing the XYZ Euler angles (in degrees).
		 /// </summary>
		 */
        void ConvertToEulerAnglesCSV(std::string &eulerAngleString) const
        {
			std::vector<float> eulerVector;
			ConvertToEulerAngles(eulerVector);
			
			std::stringstream ss;
			ss << eulerVector[0] << "," << eulerVector[1] << "," << eulerVector[2];
			eulerAngleString = ss.str();
        }
		
		/**
		 /// Converts data to string of Comma Separated Variables.
		 /// </summary>
		 */
        void ConvertToCSV(std::string &quaternionString) const
        {
			std::stringstream ss;
			ss << quaternion[0] << "," << quaternion[1] << "," << quaternion[2] << "," << quaternion[3];
			quaternionString = ss.str();
        }

	private: 
		std::vector <float> quaternion;

    };
//}
#endif // H_QUATERNIONDATA
