﻿#ifndef H_ENUMS
#define H_ENUMS

//namespace xIMU_API
//{
//    #region Compatible firmware versions

    /**
    /// Compatible firmware versions.  Only major number required.
    */
    //public 
class CompatibleFirmwareVersions 
{
public:
	enum CompatibleFirmwareVersions_
    {
        v7_x = 7
    };
};
	

//    #endregion
//
//    #region Packet headers

    /**
    /// Packet headers.
    */
    /// <remarks>
    /// A matching enumeration exists in firmware source.
    /// </remarks>
    //internal 
class PacketHeaders {
public:
	enum PacketHeaders_
    {
        ErrorMessage,
        Command,
        ReadRegister,
        WriteRegister,
        ReadDateTime,
        WriteDateTime,
        RawBattThermData,
        CalBattThermData,
        RawInertialMagneticData,
        CalInertialMagneticData,
        QuaternionData,
        DigitalIOdata,
        RawAnalogueInputData,
        CalAnalogueInputData,
        PWMoutputData,
        RawADXL345busData,
        CalADXL345busData,
        
        NumPacketHeaders
    };
};

//    #endregion
//
//    #region Error codes

    /**
    /// Error codes.
    */
    /// <remarks>
    /// A matching enumeration exists in firmware source.
    /// </remarks> 
    //public 
class ErrorCodes  
{
public:
	enum ErrorCodes_
    {
        NoError,
        FactoryResetFailed,
        LowBattery,
        USBreceiveBufferOverrun,
        USBtransmitBufferOverrun,
        BluetoothReceiveBufferOverrun,
        BluetoothTransmitBufferOverrun,
        SDcardWriteBufferOverrun,
        TooFewBytesInPacket,
        TooManyBytesInPacket,
        InvalidChecksum,
        UnknownHeader,
        InvalidNumBytesForPacketHeader,
        InvalidRegisterAddress,
        RegisterReadOnly,
        InvalidRegisterValue,
        InvalidCommand,
        GyroscopeAxisNotAt200dps,
        GyroscopeNotStationary,
        AcceleroemterAxisNotAt1g,
        MagnetometerSaturation,
        IncorrectAuxillaryPortMode,
		
		NumErrorCodes
    };
};
	

//    #endregion
//
//    #region Command codes

    /**
    /// Command codes.
    */
    /// <remarks>
    /// A matching enumeration exists in firmware source.
    /// </remarks> 
    //public 

class CommandCodes
{
public:
	enum CommandCodes_
    {
        NullCommand,
        FactoryReset,
        Reset,
        Sleep,
        ResetSleepTimer,
        SampleGyroscopeAxisAt200dps,
        CalcGyroscopeSensitivity,
        SampleGyroscopeBiasTemp1,
        SampleGyroscopeBiasTemp2,
        CalcGyroscopeBiasParameters,
        SampleAccelerometerAxisAt1g,
        CalcAccelerometerBiasAndSens,
        MeasureMagnetometerBiasAndSens,
        AlgorithmInitialise,
        AlgorithmTare,
        AlgorithmClearTare,
        AlgorithmInitialiseThenTare,
        
        NumCommandCodes
    };
};

//    #endregion
//
//    #region Fixed-point Q values

    /**
    /// Number of fractional bits used by fixed-point representations.
    */
    /// <remarks>
    /// A matching enumeration exists in firmware source.
    /// </remarks> 
    //internal 
class Qvals
{
public:
	enum Qvals_
    {
        CalibratedBatt = 12,
        CalibratedTherm = 8,
        CalibratedGyro = 4,
        CalibratedAccel = 11,
        CalibratedMag = 11,
        Quaternion = 15,
        BattSensitivity = 5,
        BattBias = 8,
        ThermSensitivity = 6,
        ThermBias = 0,
        GyroSensitivity = 7,
        GyroSampled200dps = 0,
        GyroBiasAt25degC = 3,
        GyroBiasTempSensitivity = 11,
        GyroSampledBias = 3,
        AccelSensitivity = 4,
        AccelBias = 8,
        AccelSampled1g = 4,
        MagSensitivity = 4,
        MagBias = 8,
        MagHardIronBias = 11,
        AlgorithmKp = 11,
        AlgorithmKi = 15,
        AlgorithmInitKp = 11,
        AlgorithmInitPeriod = 11,
        CalibratedAnalogueInput = 12,
        AnalogueInputSensitivity = 4,
        AnalogueInputBias = 8,
        PWMoutput = 15,
        CalibratedADXL345 = 10,
        ADXL345busSensitivity = 6,
        ADXL345busBias = 8,
        
        NumQvals
    };
};

//    #endregion
//
//    #region Register addresses

    /**
    /// Register addresses.
    */
    /// <remarks>
    /// A matching enumeration exists in firmware source.
    /// </remarks> 
    //public 
class RegisterAddresses {
public:
	enum RegisterAddresses_
    {
        FirmVersionMajorNum,
        FirmVersionMinorNum,
        DeviceID,
        ButtonMode,
        BattSensitivity,
        BattBias,
        ThermSensitivity,
        ThermBias,
        GyroFullScale,
        GyroSensitivityX,
        GyroSensitivityY,
        GyroSensitivityZ,
        GyroSampledPlus200dpsX,
        GyroSampledPlus200dpsY,
        GyroSampledPlus200dpsZ,
        GyroSampledMinus200dpsX,
        GyroSampledMinus200dpsY,
        GyroSampledMinus200dpsZ,
        GyroBiasAt25degCX,
        GyroBiasAt25degCY,
        GyroBiasAt25degCZ,
        GyroBiasTempSensitivityX,
        GyroBiasTempSensitivityY,
        GyroBiasTempSensitivityZ,
        GyroSample1Temp,
        GyroSample1BiasX,
        GyroSample1BiasY,
        GyroSample1BiasZ,
        GyroSample2Temp,
        GyroSample2BiasX,
        GyroSample2BiasY,
        GyroSample2BiasZ,
        AccelFullScale,
        AccelSensitivityX,
        AccelSensitivityY,
        AccelSensitivityZ,
        AccelBiasX,
        AccelBiasY,
        AccelBiasZ,
        AccelSampledPlus1gX,
        AccelSampledPlus1gY,
        AccelSampledPlus1gZ,
        AccelSampledMinus1gX,
        AccelSampledMinus1gY,
        AccelSampledMinus1gZ,
        MagFullScale,
        MagSensitivityX,
        MagSensitivityY,
        MagSensitivityZ,
        MagBiasX,
        MagBiasY,
        MagBiasZ,
        MagHardIronBiasX,
        MagHardIronBiasY,
        MagHardIronBiasZ,
        AlgorithmMode,
        AlgorithmKp,
        AlgorithmKi,
        AlgorithmInitKp,
        AlgorithmInitPeriod,
        AlgorithmMinValidMag,
        AlgorithmMaxValidMag,
        AlgorithmTareQuat0,
        AlgorithmTareQuat1,
        AlgorithmTareQuat2,
        AlgorithmTareQuat3,
        SensorDataMode,
        DateTimeOutputRate,
        BattThermOutputRate,
        InertialMagOutputRate,
        QuatOutputRate,
        SDcardNewFileName,
        BattShutdownVoltage,
        SleepTimer,
        MotionTrigWakeUp,
        BluetoothPower,
        AuxiliaryPortMode,
        DigitalIOdirection,
        DigitalIOoutputRate,
        AnalogueInputDataMode,
        AnalogueInputDataRate,
        AnalogueInputSensitivity,
        AnalogueInputBias,
        PWMoutputFrequency,
        ADXL345busDataMode,
        ADXL345busDataRate,
        ADXL345AsensitivityX,
        ADXL345AsensitivityY,
        ADXL345AsensitivityZ,
        ADXL345AbiasX,
        ADXL345AbiasY,
        ADXL345AbiasZ,
        ADXL345BsensitivityX,
        ADXL345BsensitivityY,
        ADXL345BsensitivityZ,
        ADXL345BbiasX,
        ADXL345BbiasY,
        ADXL345BbiasZ,
        ADXL345CsensitivityX,
        ADXL345CsensitivityY,
        ADXL345CsensitivityZ,
        ADXL345CbiasX,
        ADXL345CbiasY,
        ADXL345CbiasZ,
        ADXL345DsensitivityX,
        ADXL345DsensitivityY,
        ADXL345DsensitivityZ,
        ADXL345DbiasX,
        ADXL345DbiasY,
        ADXL345DbiasZ,
        UARTbaudRate,
        UARThardwareFlowControl,
        
        NumRegisters
    };
	
};
//    #endregion
//
//    #region Register values

    /**
    /// Button mode register values.
    */
    /// <remarks>
    /// A matching enumeration exists in firmware source.
    /// </remarks> 
    //public 

class ButtonModes 
{
public:
	enum ButtonModes_
    {
        Disabled,
        Reset,
        SleepWake,
        AlgorithmInitialise,
        AlgorithmTare,
        AlgorithmInitialiseThenTare
    };
};

/**
/// Accelerometer full-scale register values.
 */
/// </summary>
/// <remarks>
/// A matching enumeration exists in firmware source.
/// </remarks> 

class GyroscopeFullScales
{
public:    
    enum GyroscopeFullScales_
    {
        FullScalePlusMinus250dps,
        FullScalePlusMinus500dps,
        FullScalePlusMinus1000dps,
        FullScalePlusMinus2000dps
    };
};
    /**
    /// Accelerometer full-scale register values.
    */
    /// <remarks>
    /// A matching enumeration exists in firmware source.
    /// </remarks> 
    //public 
class AccelerometerFullScales 
{
public:
	enum AccelerometerFullScales_
    {
        PlusMinus2g,
        PlusMinus4g,
        PlusMinus8g
    };
};

    /**
    /// Magnetometer full-scale register values.
    */
    /// <remarks>
    /// A matching enumeration exists in firmware source.
    /// </remarks> 
    //public 
class MagnetometerFullScales 
{
public:
	enum MagnetometerFullScales_
    {
        PlusMinus1p3G,
        PlusMinus1p9G,
        PlusMinus2p5G,
        PlusMinus4p0G,
        PlusMinus4p7G,
        PlusMinus5p6G,
        PlusMinus8p1G
    };
};

    /**
    /// Algorithm mode register values.
    */
    /// <remarks>
    /// A matching enumeration exists in firmware source.
    /// </remarks> 
    //public 
class AlgorithmModes 
{
public:
	enum AlgorithmModes_
    {
        Disabled,
        IMU,
        AHRS
    };
};
    /**
    /// Data mode register values.
    */
    /// <remarks>
    /// A matching enumeration exists in firmware source.
    /// </remarks> 
    //public 
class SensorDataModes 
{
    public:
	enum SensorDataModes_
    {
        Raw,
        Calibrated,
    };
};
	

    /**
    /// Date/time data output rate register values.
    */
    /// <remarks>
    /// A matching enumeration exists in firmware source.
    /// </remarks> 
	//public
class DateTimeOutputRates 
{
public:
    enum DateTimeOutputRates_
    {
        OnResetOnly,
        Rate1Hz,
    };
};

    /**
    /// Data output rate register values.
    */
    /// <remarks>
    /// A matching enumeration exists in firmware source.
    /// </remarks> 
    //public 
class DataOutputRates
{
    public:
	enum DataOutputRates_
    {
        Disabled,
        Rate1Hz,
        Rate2Hz,
        Rate4Hz,
        Rate8Hz,
        Rate16Hz,
        Rate32Hz,
        Rate64Hz,
        Rate128Hz,
        Rate256Hz
    };
};

    /**
    /// Motion triggered wake up mode register values.
    */
    /// <remarks>
    /// A matching enumeration exists in firmware source.
    /// </remarks> 
    //public
class MotionTrigWakeUpModes 
{
    public:
	enum MotionTrigWakeUpModes_
    {
        Disabled,
        LowSensitivity,
        HighSensitivity
    };
};

    /**
    /// Bluetooth power mode register values.
    */
    /// <remarks>
    /// A matching enumeration exists in firmware source.
    /// </remarks> 
    //public
class BluetoothPowerModes
{
    public:
	enum BluetoothPowerModes_
    {
        Disabled,
        Enabled
    };
};

    /**
    /// Auxiliary Port mode register values.
    */
    /// <remarks>
    /// A matching enumeration exists in firmware source.
    /// </remarks> 
    //public
class AuxiliaryPortModes 
{
    public:
	enum auxiliaryPortModes_
    {
        Disabled,
        DigitalIO,
        AnalogueInput,
        PWMoutput,
        ADXL345bus
    };
};
    /**
    /// Digital I/O direction register values.
    */
    /// <remarks>
    /// A matching enumeration exists in firmware source.
    /// </remarks> 
    //public
class DigitalIOdirections 
{
    public:
	enum DigitalIOdirections_
    {
        In01234567,
        In0123456Out7,
        In012345Out67,
        In01234Out567,
        In0123Out4567,
        In012Out34567,
        In01Out234567,
        In0Out1234567,
        Out01234567
    };
};

//    #endregion
//}
	
#endif //H_ENUMS
