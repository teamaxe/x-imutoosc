/*
  ==============================================================================

    SerialControls.h
    Created: 19 Aug 2015 10:19:23am
    Author:  Tom Mitchell

  ==============================================================================
*/

#ifndef SERIALCONTROLS_H_INCLUDED
#define SERIALCONTROLS_H_INCLUDED

#include "SerialPort.h"

/** Class containing menu, logic and connect button for a @SerialPort */
class SerialControls :  public Component,
                        private Button::Listener
{
public:
    /** construct */
    SerialControls (SerialPort& serialPort_) : serialPort (serialPort_)
    {
        connectButton.setButtonText ("Connect");
        connectButton.setConnectedEdges (Button::ConnectedOnLeft | Button::ConnectedOnRight);
        connectButton.addListener (this);
        addAndMakeVisible (&connectButton);
        
        addAndMakeVisible (serialCombobox);
    }
    /** destruct */
    ~SerialControls()
    {
        connectButton.removeListener (this);
    }
    //Component
    void resized() override
    {
        Rectangle<int> r (getLocalBounds());
        serialCombobox.setBounds (r.removeFromLeft ((int)(r.getWidth() * 0.85)));
        connectButton.setBounds (r);
    }
    //Button::Listener
    void buttonClicked (Button* button) override
    {
        if (button == &connectButton)
        {
            if (connectButton.getToggleState())
            {
                serialPort.closePort (serialCombobox.getItemText (serialCombobox.getSelectedId() - 1));
                connectButton.setToggleState (false, dontSendNotification);
            }
            else
            {
                SerialPort::SerialPortSettings sps (serialCombobox.getItemText (serialCombobox.getSelectedId() - 1), SerialPort::Baud115200);
                serialPort.openPort (sps);
                connectButton.setToggleState (true, dontSendNotification);
            }
        }
    }
private:
/** class that repopulates the device list everytime the device dropdown is clicked - so new devices appear without having to restart */
class SerialComboBox : public ComboBox
{
public:
    SerialComboBox()
    {
        populateDeviceList();
        int selectedPort = -1;
        for (int i = 0; i < getNumItems(); i++)
            if (getItemText(i).contains ("serial"))
                selectedPort = i + 1;
        if (selectedPort >= 0)
            setSelectedId (selectedPort);
    }
    void populateDeviceList()
    {
        String previouslySelectedDevice (getItemText (getSelectedId() - 1));
        StringArray ports;
        SerialPort::getDevicePathList (ports);
        clear (dontSendNotification);
        addItemList (ports, 1);
        
        if ( ! previouslySelectedDevice.isEmpty())
        {
            int selectedPort = -1;
            for (int i = 0; i < getNumItems(); i++)
                if (getItemText (i) == previouslySelectedDevice)
                    selectedPort = i + 1;
            if (selectedPort >= 0)
                setSelectedId (selectedPort);
        }
    }
    void showPopup() override
    {
        populateDeviceList();
        ComboBox::showPopup();
    }
};
    TextButton connectButton;
    SerialComboBox serialCombobox;
    SerialPort& serialPort;
};

#endif  // SERIALCONTROLS_H_INCLUDED
