/*
 *  SerialPort.h
 *  sdaJuce
 *  Created by tjmitche on 19/05/2011.
 *  Copyright 2011 __MyCompanyName__. All rights reserved.
 */

#ifndef H_TLIB_SERIALPORT
#define H_TLIB_SERIALPORT

////////////

#include "../JuceLibraryCode/JuceHeader.h"
#include <iostream>
#include <string>

#if JUCE_WINDOWS
#include <windows.h>
#else
#include <termios.h>
#include <fcntl.h>
#endif

#if JUCE_WINDOWS
typedef HANDLE SerialDescriptor;
#else
typedef int SerialDescriptor;
#endif

#if JUCE_WINDOWS
#else
#endif

/**
 Class to manage serial port connections.

 Todo: 
 - Remove all the commented code
 - Do something with the errormessages - they can't be returned currently
 - Test fine control of the serial port settings!
 - When reading enable timeout, bufferlength or frame termination to control when callbacks are made
 - Add control for nonstandard baud rates
 */

class SerialPort : private Thread
{
public:
    /** 
        Populates the provided @StringArray with all availabe serial ports on the machine.
        If anything goes wrong it will return a fail Result and error message
     */
    static Result getDevicePathList (StringArray& devicePathList);
    
    /** Baudrate constants for serial port settings */
    enum BaudRate
    {
		#if JUCE_WINDOWS
		Baud110		= CBR_110,    
		Baud300		= CBR_300,    
		Baud600		= CBR_600,    
		Baud1200    = CBR_1200,
		Baud2400    = CBR_2400,
		Baud4800    = CBR_4800,
		Baud9600    = CBR_9600,
		Baud14400   = CBR_14400,
		Baud19200   = CBR_19200,
		Baud38400   = CBR_38400,
		Baud56000   = CBR_56000,  
		Baud57600   = CBR_57600,  
		Baud115200	= CBR_115200,
		Baud128000  = CBR_128000, 
		Baud256000  = CBR_256000
		#else
        Baud50		= B50,
        Baud75		= B75,
        Baud110		= B110,
        Baud134		= B134,
        Baud150		= B150,
        Baud200		= B200,
        Baud300		= B300,
        Baud600		= B600,
        Baud1200	= B1200,
        Baud1800	= B1800,
        Baud2400	= B2400,
        Baud4800	= B4800,
        Baud7200	= B7200,
        Baud9600	= B9600,
        Baud14400	= B14400,
        Baud19200	= B19200,
        Baud28800	= B28800,
        Baud38400	= B38400,
        Baud57600	= B57600,
        Baud76800	= B76800,
        Baud115200	= B115200,
        Baud230400	= B230400
		#endif
        
    };
    
    /** Read / write constants for serial port settings */
    enum ReadWriteAccess
    {
		#if JUCE_WINDOWS
		ReadOnly        = GENERIC_READ,
		WriteOnly       = GENERIC_WRITE,
		ReadAndWrite	= GENERIC_READ | GENERIC_WRITE
		#else
		ReadOnly        = O_RDONLY,
        WriteOnly       = O_WRONLY,
        ReadAndWrite    = O_RDWR
		#endif
        
    };
    
    /** Databits constants for serial port settings */
    enum DataBits
    {
		#if JUCE_WINDOWS
		DataBits5 = 5,
        DataBits6 = 6,
        DataBits7 = 7,
        DataBits8 = 8
		#else
		DataBits5 = CS5,
        DataBits6 = CS6,
        DataBits7 = CS7,
        DataBits8 = CS8
		#endif
    };
    
    /** Flow control constants for serial port settings */
    enum FlowControl
    {
        NoFlowControl = 0,
        EnableHardwareFlowControl = 1,
        EnableSoftwareFlowContorl = 2,
        EnableBothSoftwareAndHardwareFlowControl = EnableHardwareFlowControl | EnableSoftwareFlowContorl
    };

	/** Parity constants for serial port settings */
    enum StopBits
    {
		#if JUCE_WINDOWS
		OneStopBit = ONESTOPBIT,
        TwoStopBit = TWOSTOPBITS
		#else
		OneStopBit,
        TwoStopBit
		#endif
    };
    
    /** Parity constants for serial port settings */
    enum Parity
    {
		#if JUCE_WINDOWS
		NoParity = NOPARITY,
        EvenParity = EVENPARITY,
        OddParity = ODDPARITY
		#else
		NoParity,
        EvenParity,
        OddParity
		#endif

    };
    
    /** struct for fine control of serial port settings, use defaultSettings for the defaults... */
    struct SerialPortSettings
    {
        SerialPortSettings (const String&   portName_ = String::empty,
                            BaudRate        br        = Baud115200,
                            ReadWriteAccess rwa       = ReadAndWrite,
                            DataBits        db        = DataBits8,
                            FlowControl     fc        = NoFlowControl,
                            StopBits        sb        = OneStopBit,
                            Parity          pty       = NoParity)
         :  portName (portName_), 
            readwriteAccess (rwa), 
            baudRate (br), 
            dataBits (db), 
            flowControl (fc), 
            stopBits (sb),
            parity (pty),
            #if JUCE_WINDOWS
            descriptor (INVALID_HANDLE_VALUE)
            #else
            descriptor (-1)
            #endif
        {
            
        }
        
        //Settings
        inline const String&   getPortName()           const {return portName;}
        inline ReadWriteAccess getReadWriteAccess()    const {return readwriteAccess;}
        inline BaudRate        getBaudRate()           const {return baudRate;}
        inline DataBits        getDataBits()           const {return dataBits;}
        inline FlowControl     getFlowControl()        const {return flowControl;}
        inline StopBits        getStopBits()           const {return stopBits;}
        inline Parity          getParity()             const {return parity;}
    
        inline void setDescriptor (SerialDescriptor value) {descriptor = value;}
        inline SerialDescriptor getDescriptor () const {return descriptor;}
        private:
            String            portName;
            ReadWriteAccess   readwriteAccess;
            BaudRate          baudRate;
            DataBits          dataBits;
            FlowControl       flowControl;
            StopBits          stopBits;
            Parity            parity;
            SerialDescriptor  descriptor;
    };
    
    /** Constructor */
	SerialPort(); 
    
    /** Destructor */
	~SerialPort();
    
    /**
     Asyncronously opens a port with the provided @SerialPortSettings.
     This function returns imediately opening happens in the serial port thread
     
     Multiple calls can be made for opening several ports simultaniously. There is only one searial thread that services them all.
     
     @param settings  settings for the port to be opened
     */
    void openPort (const SerialPort::SerialPortSettings& settings);
    
//    /**
//     Closes the port at the specified index, if this is the only port with the read mode flag set the read thread will be stopped.
//     
//     @returns a fail message if anything goes wrong.
//     */
//    Result closePort (unsigned int portNumber);
    
    /**
     Closes the named port, if this is the only port with the read mode flag set the read thread will be stopped.
     
     @returns a fail message if anything goes wrong.
     */
    void closePort (const String& nameOfPortToClose);
    
    /**
     Closes all ports, the read thread stopped.
     
     @returns a fail message if anything goes wrong.
     */
    void closeAllPorts();
    
    /** Returns true if there is a port open with the read flag set */
    bool isReading() const;
    
    /**
     Asyncronously writes the data on the specified port.
     This function returns imediately opening happens in the serial port thread
     */
    void writeToPort (const String& nameOfPortToWriteTo, const char* const data, unsigned int dataSize);
    
	
    ////////////////Listeners
	/**
     Class for a listener to this serial port connection to inherit. Only one listener at present.
     */
	class Listener
    {
    public:
        /** Destructor. */
        virtual ~Listener()                                     {}
		
        /** Called when the a data packet is recieved. */
        //virtual void serialDataRecieved (const int portIndex, const uint8 byte) = 0;
        virtual void serialDataRecieved (const String& portName, const uint8 byte) = 0;
        /** Called when the connection fails */
        virtual void serialConnectionError(const unsigned int errorCode) = 0;
        /** Called when the connection timesout */
        virtual void serialPortTimeout() = 0;
    };
	
    /** adds the listener to recieve bytes as they are recieved. Only one at a time at the moment. */
	void addListener(SerialPort::Listener* const listenerToAdd);
	
    /** removes the listener so that it will no londger recieve messages. */
	void removeListener(SerialPort::Listener* const listenerToRemove);
    
private:
    
    /** Sets up a port ready for reading and writing this should only be called from the serial thread */
    Result initialisePort (SerialPort::SerialPortSettings& settings);
    /** Sets up a port ready for reading and writing this should only be called from the serial thread */
    Result releasePort (SerialPort::SerialPortSettings& settings);
    /** wait on and read from any open serail ports */
    void waitAndRead();
    
    /** struct for queue items when writing data */
    struct WriteData
    {
        WriteData () {}
        WriteData (const String& portName, const char* const data, unsigned int dataSize)
        :  portToWriteTo (portName), dataToWrite (data, dataSize) {}
        String portToWriteTo;
        MemoryBlock dataToWrite;
    };
    
    /** write any pending serial messages */
    void writeQueuedMessages();
    /** Initialise ports queued in portsToOpen and move them to the openPortSettings array and update internal state */
    void configurePortsToOpen();
    /** release the ports queued in portsToClose and remove them to the openPortSettings array and update internal state*/
    void configurePortsToClose();

    /** recalculates imprtant parameters for all ports - should be called internally whenever a port is opened or close */
    void configureGlobalSettings();
    
    /** read a byte of raw data. If anything goes wrong it will return a non-zero @SerialPort::ErrorCode */
    unsigned int readByte(unsigned char *byte);
	
	/** read a block of raw data - returns 1 if successful and 0 if not. If the return value == 0 something's gone wrong. */
    //	int readBlock(unsigned char *buf, size_t size);
    
    //Thread
	void run();
    
    /////////////
    CriticalSection lock; //used to synconrise access for all members accessible on multiple threads
    
    Array <SerialPortSettings> openPortSettings;    //stores details for all open ports
    Array <SerialPortSettings> portsToOpen;         //producer storage used for port open requests
    Array <String> portsToClose;                    //producer storage used for port close requests
    Array <WriteData> dataToWrite;                  //queue for data to be written in the serial thread
    
    bool readState;
    int descriptorMax;                              //required by select() 'IX only
    //////////
    
	ListenerList<Listener> listeners; //for our only listener
	
	JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (SerialPort);
};

#endif // H_TLIB_SERIALPORT