//
//  ToggleButtonHierarchy.h
//  x-IMUtoOSC
//
//  Created by Tom Mitchell on 19/10/2015.
//
//

#ifndef H_ImuButtons
#define H_ImuButtons

#include "../JuceLibraryCode/JuceHeader.h"

class SensorButtons :   public Component,
                        public Button::Listener
{
public:
    SensorButtons (const String& buttonText)
    {
        addButton (buttonText);
    }
    
    bool getState() const
    {
        return (bool)toggleStates.getReference(0).get();
    }
    
    //Component
    void resized() override
    {
        Rectangle<int> r (getLocalBounds());
        const int buttonWidth = r.getWidth() / 5;
        for (int i = 0; i < buttons.size(); i++)
            buttons.getUnchecked (i)->setBounds (r.removeFromLeft (i == 0 ? buttonWidth * 2 : buttonWidth));
    }
    
    //Button::Listener
    void buttonClicked (Button* button) override
    {
        int i = 0;
        while (button != buttons.getUnchecked (i)) i++;
        if (i < buttons.size())
        {
            toggleStates.getReference (i) = buttons.getUnchecked (i)->getToggleState();
            if (i == 0)
                for (int i = 1; i < buttons.size(); i++)
                    buttons.getUnchecked (i)->setEnabled ((bool)toggleStates.getReference (0).get());
        }
    }
protected:
    void addButton (const String& buttonText)
    {
        TextButton* but = new TextButton(buttonText);
        but->setClickingTogglesState (true);
        but->setToggleState (true, sendNotification);
        but->setConnectedEdges (Button::ConnectedOnLeft | Button::ConnectedOnRight);
        but->addListener (this);
        addAndMakeVisible (but);
        
        buttons.add (but);
        toggleStates.add (Atomic<int>((int)true));
    }
    
    OwnedArray<TextButton> buttons;
    Array < Atomic<int> >  toggleStates;
};

class ImuButtons : public SensorButtons
{
public:
    ImuButtons () : SensorButtons ("imu")
    {
        addButton ("quaternion");
        addButton ("rotation");
        addButton ("euler");
    }
    
    bool getQuaternionState() const
    {
        return (bool)toggleStates.getReference(1).get();
    }
    
    bool getRotationState() const
    {
        return (bool)toggleStates.getReference(2).get();
    }
    
    bool getEulerState() const
    {
        return (bool)toggleStates.getReference(3).get();
    }
    
    //Component
    void resized() override
    {
        Rectangle<int> r (getLocalBounds());
        const int buttonWidth = r.getWidth() / 5;
        for (int i = 0; i < buttons.size(); i++)
            buttons.getUnchecked (i)->setBounds (r.removeFromLeft (i == 0 ? buttonWidth * 2 : buttonWidth));
    }
};

class InertialButtons : public SensorButtons
{
public:
    InertialButtons () : SensorButtons ("inertial")
    {
        addButton ("accelerometer");
        addButton ("gyroscope");
        addButton ("magnetometer");
    }
    
    bool getAccelerometerState() const
    {
        return (bool)toggleStates.getReference(1).get();
    }
    
    bool getGyroscopeState() const
    {
        return (bool)toggleStates.getReference(2).get();
    }
    
    bool getMagnetometerState() const
    {
        return (bool)toggleStates.getReference(3).get();
    }
    
    //Component
    void resized() override
    {
        Rectangle<int> r (getLocalBounds());
        const int buttonWidth = r.getWidth() / 5;
        for (int i = 0; i < buttons.size(); i++)
            buttons.getUnchecked (i)->setBounds (r.removeFromLeft (i == 0 ? buttonWidth * 2 : buttonWidth));
    }
};

class DigitalButtons : public SensorButtons
{
public:
    DigitalButtons() : SensorButtons ("digital")
    {

    }
    
    
    //Component
    void resized() override
    {
        Rectangle<int> r (getLocalBounds());
        const int buttonWidth = r.getWidth() / 5;
        for (int i = 0; i < buttons.size(); i++)
            buttons.getUnchecked (i)->setBounds (r.removeFromLeft (i == 0 ? buttonWidth * 2 : buttonWidth));
        }
};

#endif /* H_ToggleButtonHierarchy */
