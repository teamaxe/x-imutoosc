/*
  ==============================================================================

    OscMessage.cpp
    Created: 8 Sep 2012 1:25:05pm
    Author:  tjmitche

  ==============================================================================
*/

#include "OscMessage.h"

OscMessage::OscMessage (const char* const rawData, const int rawDataLength) 
{
    if(parseRawData (rawData, rawDataLength) != false)
        DBG("Error parsing raw OSC message");
}

bool OscMessage::parseRawData (const char* const rawData, const int rawDataLength)
{
//        std::cout << "************\n";
//        for (int i = 0; i < rawDataLength; i++)
//            std::cout << (char)rawData[i] << "\n";
//        std::cout << "************\n";
//        std::cout << "************\n";
//        for (int i = 0; i < rawDataLength; i++)
//            std::cout << rawData[i] << "\n";
//        std::cout << "************\n";
    
    if (rawDataLength && rawDataLength % 4 == 0) //check for > 0 bytes and multiple of 4 bytes
    {
        int pos = 0;
        
        //weak bundle handling
        if (rawData[pos] == '#')
            pos += 20;
        
        //copy the address into the address string
        while (true)
        {
            address += rawData[pos];
            
            if (rawData[pos] == '\0') //copy until end of address string
                break;
            
            if (++pos == rawDataLength) //if we've reached the end of the message without finding a null
            {
                clear();
                return true;
            }
        }
        
        //advance over the null
        if (++pos == rawDataLength) //if we've reached the end of the message without finding a null
        {
            clear();
            return true;
        }
        
        //Advance to the typetag
        while (pos % 4 != 0)
        {
            if (++pos == rawDataLength) //if we've reached the end of the message without finding a null
            {
                clear();
                return true;
            }
        }
        //check for comma at the start of the type tag
        if (rawData[pos] != ',') //if this is not a comma
        {
            address = String::empty;
            return true;
        }
        
        //advance over the ','
        if (++pos == rawDataLength) //if we've reached the end of the message without finding a null
        {
            clear();
            return true;
        }
        
        //copy the typetag into the typetag string
        while (true)
        {
            typeTag += rawData[pos];
            
            if (rawData[pos] == '\0') //copy until end of typetag
                break;
            
            //skip over comma first time and increment the rest of the time.
            if (++pos == rawDataLength) //if we've reached the end of the message without finding a null
            {
                clear();
                return true;
            }
        }
        
        //advance over the 'null'
        if (++pos > rawDataLength) //if we've reached the end of the message without finding a null
        {
            clear();
            return true;
        }
        
        //Advance to the arguments
        while (pos % 4 != 0)
        {
            if (++pos > rawDataLength) //this means we have no arguments!
            {
                clear();
                return true;
            }
        }
        
        //extract all arguments
        for (int i = 0; i < typeTag.length(); i++)
        {
            switch (typeTag.getCharPointer()[i])
            {
                case 'i':
                {
                    if (pos + 4 > rawDataLength)
                    {
                        clear();
                        return true;
                    }
                    else
                    {
                        Argument32 arg;
                        arg.c[3] = rawData[pos++];
                        arg.c[2] = rawData[pos++];
                        arg.c[1] = rawData[pos++];
                        arg.c[0] = rawData[pos++];
                        arguments.add (var (arg.intValue));
                    }
                    break;
                }
                case 'f':
                {
                    if (pos + 4 > rawDataLength)
                    {
                        clear();
                        return true;
                    }
                    else
                    {
                        Argument32 arg;
                        arg.c[3] = rawData[pos++];
                        arg.c[2] = rawData[pos++];
                        arg.c[1] = rawData[pos++];
                        arg.c[0] = rawData[pos++];
                        arguments.add (var (arg.floatValue));
                    }
                    break;
                }
                case 's':
                {
                    String temp;
                    while (true)
                    {
                        temp += rawData[pos];
                        if (rawData[pos] == '\0')
                            break;
                        
                        if (++pos > rawDataLength) //this means that the string is missing
                        {
                            clear();
                            return true;
                        }
                    }
                    arguments.add (temp);
                    
                    //advance over the 'null'
                    if (++pos > rawDataLength) //if we've reached the end of the message without finding a null
                    {
                        clear();
                        return true;
                    }
                    
                    //Advance to next arguments
                    while (pos % 4 != 0)
                    {
                        if (++pos > rawDataLength) //this means we have no arguments!
                        {
                            clear();
                            return true;
                        }
                    }
                    break;
                }
                case 'b':
                {
                    if (pos + 4 > rawDataLength)
                    {
                        clear();
                        return true;
                    }
                    else
                    {
                        Argument32 arg;
                        arg.c[3] = rawData[pos++];
                        arg.c[2] = rawData[pos++];
                        arg.c[1] = rawData[pos++];
                        arg.c[0] = rawData[pos++];
                        
                        if (pos + arg.intValue > rawDataLength)//check for missmatch between reported size and remainigng message length
                        {
                            clear();
                            return true;
                        }
                        arguments.add (var (&rawData[pos], arg.intValue));
                        pos += arg.intValue;
                        
                        //Advance to next arguments
                        while (pos % 4 != 0)
                        {
                            if (++pos > rawDataLength) //this means we have no arguments!
                            {
                                clear();
                                return true;
                            }
                        }
                    }
                    break;
                }
                case 'T':
                case 'F':
                {
                    arguments.add (var (typeTag.getCharPointer()[i] == 'T'));
                    break;
                }
                default:
                {
                    clear();
                    return true;
                    break;
                }
            }
        }
    }
    return false;
}

bool OscMessage::isEmpty() const
{
    return address.isEmpty();
}

void OscMessage::clear()
{
    address = typeTag =  String::empty;
    arguments.clear();
}

String OscMessage::asString()
{
    String messageString (address + " ");
    for (int i = 0; i < getNumberOfArguments(); i++)
    {
        switch (getArgumentType(i))
        {
            case Int32Type: messageString << String (getArgumentAsInt32(i)) + " "; break;
            case Float32Type: messageString << String (getArgumentAsFloat32(i)) + " "; break;
            case StringType: messageString << getArgumentAsString(i) + " "; break;
            case BlobType: messageString << String ((CharPointer_UTF8)getArgumentAsBlob(i)) + " "; break;
            case BoolType: messageString << String (getArgumentAsBlob(i)) + " "; break;
                
            default:
                break;
        }
    }
    return messageString;
}

const String& OscMessage::getAddress () const
{
    return address;
}

int OscMessage::getNumberOfArguments() const
{
    return arguments.size();
}

OscMessage::ArgumentType OscMessage::getArgumentType (unsigned int index) const
{
    if (index < getNumberOfArguments())
    {
        switch (typeTag.getCharPointer()[index])
        {
            case 'i':   return Int32Type; break;
            case 'f':   return Float32Type; break;
            case 'b':   return BlobType; break;
            case 's':   return StringType; break;
            case 'F':
            case 'T':   return BoolType; break;
        }
    }
    return UnknownType;
}

int OscMessage::getArgumentAsInt32(unsigned int index) const
{
    if (index < getNumberOfArguments() && arguments.getReference(index).isInt())
        return arguments.getReference(index);
    else
        return 0;
}

float OscMessage::getArgumentAsFloat32 (unsigned int index) const
{
    if (index < getNumberOfArguments() && arguments.getReference(index).isDouble())
        return arguments.getReference(index);
    else
        return 0.f;
}

const String OscMessage::getArgumentAsString(unsigned int index) const
{
    if (index < getNumberOfArguments() && arguments.getReference(index).isString())
        return arguments.getReference(index).toString();
    else
        return String::empty;
}

const char* OscMessage::getArgumentAsBlob (unsigned int index) const
{
    if (index < getNumberOfArguments() && arguments.getReference(index).isBinaryData())
        return static_cast <const char *>(arguments.getReference(index).getBinaryData()->getData());
    else
        return nullptr;
}

int OscMessage::getBlobSize(unsigned int index) const
{
    if (index < getNumberOfArguments() && arguments.getReference(index).isBinaryData())
        return arguments.getReference(index).getBinaryData()->getSize();
    else
        return 0;
}

bool OscMessage::getArgumentAsBool (unsigned int index) const
{
    if (index < getNumberOfArguments() && arguments.getReference(index).isBool())
        return arguments.getReference(index);
    else
        return false;
}

const size_t OscMessage::getRawDataSize()
{
    size_t memoryRequired = address.length() + 1;  //+null teminator
    if (memoryRequired % 4 != 0)
        memoryRequired += 4 - memoryRequired % 4;
    //typetag
    memoryRequired += typeTag.length() + 2;     //+ comma + null terminator
    if (memoryRequired % 4 != 0)
        memoryRequired += 4 - memoryRequired % 4;
    
    //arguments
    for (int i = 0; i < typeTag.length(); i++)
    {
        switch (getArgumentType(i))
        {
            case Int32Type:
            case Float32Type:
                memoryRequired += 4;
                break;
                
            case StringType:
                memoryRequired += arguments[i].toString().length() + 1;  //size plus the size int at the start
                if (memoryRequired % 4 != 0)                         //plus padding
                    memoryRequired += 4 - memoryRequired % 4;
                break;
                
            case BlobType:
                memoryRequired += arguments[i].getBinaryData()->getSize() + 4;   //size plus the size int at the start
                if (memoryRequired % 4 != 0)                            //plus padding
                    memoryRequired += 4 - memoryRequired % 4;
                break;
                
            case BoolType: /* no additional size */
                break;
                
            default:
                break;
        }
    }
    
    return memoryRequired;
}

const char* const OscMessage::getRawData()
{
    if (isEmpty())
        return nullptr;
    
    //address
    uint memoryRequired = getRawDataSize();
    
    //allocateRawMemory
    rawData.setSize (memoryRequired, false);
    
    //        DBG("****SizeRequested:" << String(memoryRequired) << " SizeGot:" << String((int)rawData.getSize()));
    
    //write data
    uint writePosition = 0;
    
    //address
    rawData.copyFrom (address.getCharPointer(), writePosition, address.length());
    writePosition += address.length();
    rawData[writePosition++] = '\0';
    
    //change this to a single while loop?
    if (writePosition % 4 != 0)
    {
        int nullCount = 4 - writePosition % 4;
        //            printf("nullPlant! %d %d, %d\n\n", writePosition, writePosition%4, nullCount);
        for (int i = 0; i < nullCount; i++)
            rawData[writePosition++] = '\0';
    }
    
    //typetag
    rawData[writePosition++] = ',';
    rawData.copyFrom(typeTag.getCharPointer(), writePosition, typeTag.length());
    writePosition += typeTag.length();
    rawData[writePosition++] = '\0';
    
    if (writePosition % 4 != 0)
    {
        int nullCount = 4 - writePosition % 4;
        for (int i = 0; i < nullCount; i++)
            rawData[writePosition++] = '\0';
    }
    
    //Arguments
    for (int i = 0; i < getNumberOfArguments(); i++)
    {
        //ArgumentType type = getArgumentType(i);
        switch (getArgumentType(i))
        {
            case Int32Type:
            {
                Argument32 arg;
                arg.intValue = arguments.getReference(i);
                rawData[writePosition++] = arg.c[3];
                rawData[writePosition++] = arg.c[2];
                rawData[writePosition++] = arg.c[1];
                rawData[writePosition++] = arg.c[0];
                break;
            }
            case Float32Type:
            {
                Argument32 arg;
                arg.floatValue = arguments.getReference(i);
                rawData[writePosition++] = arg.c[3];
                rawData[writePosition++] = arg.c[2];
                rawData[writePosition++] = arg.c[1];
                rawData[writePosition++] = arg.c[0];
                break;
            }
            case StringType:
            {
                rawData.copyFrom(arguments.getReference(i).toString().getCharPointer(), writePosition, arguments.getReference(i).toString().length());
                writePosition += arguments.getReference(i).toString().length();
                rawData[writePosition++] = '\0';
                
                if (writePosition % 4 != 0)
                {
                    int nullCount = 4 - writePosition % 4;
                    for (int i = 0; i < nullCount; i++)
                        rawData[writePosition++] = '\0';
                }
                break;
            }
            case BlobType:
            {
                const char* dataPtr = static_cast<const char*>(arguments[i].getBinaryData()->getData());
                rawData[writePosition++] = dataPtr[3];
                rawData[writePosition++] = dataPtr[2];
                rawData[writePosition++] = dataPtr[1];
                rawData[writePosition++] = dataPtr[0];
                
                rawData.copyFrom(dataPtr, writePosition, arguments[i].getBinaryData()->getSize());
                writePosition += arguments[i].getBinaryData()->getSize();
                if (writePosition % 4 != 0)
                {
                    int nullCount = 4 - writePosition % 4;
                    for (int i = 0; i < nullCount; i++)
                        rawData[writePosition++] = '\0';
                }
                break;
            }
            case BoolType: // nada
                break;
                
            default:
                break;
                
        }
    }
    
    return static_cast<char*>(rawData.getData());
}

void OscMessage::setAddress (const String& newAddress)
{
    address = newAddress;
}

void OscMessage::addAddress (const String& addAddress)
{
    address += addAddress;
}

void OscMessage::addFloat32Argument (const float value)
{
    typeTag += 'f';
    arguments.add (var (value));
}

void OscMessage::addInt32Argument (const int32 value)
{
    typeTag += 'i';
    arguments.add (var (value));
}

void OscMessage::addStringArgument (const String& string)
{
    typeTag += 's';
    arguments.add (var (string));
}

void OscMessage::addBlobArgument (const unsigned char* const blob, const unsigned int blobSize)
{
    typeTag += 'b';
    arguments.add (var (blob, blobSize));
}

void OscMessage::addBoolArgument (const bool state)
{
    typeTag += state ? 'T' : 'F';
    arguments.add (var (state));
}

#ifdef DOUNITTESTS

class OscMessageTest : public UnitTest
{
public:
    OscMessageTest() : UnitTest("OscMessage") {};
    
    void runTest()
    {
        //====================================================
        beginTest ("Testing OscMessage parsing for failiers");
        
        {
            const char data[] = {'\0'};// empty message?
            OscMessage mes (data, 0);
            expect(mes.isEmpty() == true, "empty message parse error");
        }
        
        {
            const char data[] = {'/','t'};// invalid length not null terminated
            OscMessage mes (data, sizeof(data));
            expect(mes.isEmpty() == true, "message length error");
        }
        {
            const char data[] = {'/','t','e','s'};// address not null teminated
            OscMessage mes (data, sizeof(data));
            expect(mes.isEmpty() == true, "error parsing address");
        }
        {
            const char data[] = {'/','t','e','\0'};// address and null with message size 4
            OscMessage mes (data, sizeof(data));
            expect(mes.isEmpty() == true, "error parsing address");
        }
        {
            const char data[] = {'/','t','e','s','t','\0','\0','\0'};// missing type tag
            OscMessage mes (data, sizeof(data));
            expect(mes.isEmpty() == true, "error parsing address");
        }
        {
            const char data[] = {'/','t','e','s','t','\0','\0','\0','\0','\0','\0','\0'}; //missing type tag comma
            OscMessage mes (data, sizeof(data));
            expect(mes.isEmpty() == true, "error parsing missing comma data");
        }
        {
            const char data[] = {'/','t','e','s','t','\0','\0','\0',',','i','i','i'}; //missing type tag comma
            OscMessage mes (data, sizeof(data));
            expect(mes.isEmpty() == true, "error breaking from finding comma");
        }
        
        {
            const char data[] = {'/','t','e','s','t','\0','\0','\0',',','f','i','\0'}; //missing arguments
            OscMessage mes (data, sizeof(data));
            expect(mes.isEmpty() == true, "error parsing missing arguments test");
        }
        
        {
            const char data[] = {'/','t','e','s','t','\0','\0','\0',',','s','\0','\0'}; //missing arguments
            OscMessage mes (data, sizeof(data));
            expect(mes.isEmpty() == true, "error parsing missing arguments test");
        }
        {
            const char data[] = {'/','t','e','s','t','\0','\0','\0',',','s','\0','\0','h','i','i','i'}; //missing arguments
            OscMessage mes (data, sizeof(data));
            expect(mes.isEmpty() == true, "error parsing unterminated string argument");
        }
        
        beginTest ("Testing OscMessage parsing for success");
//

        
        {
            const char data[] = {   '/','s','e','r','i','a','l','o','s','c','/','d','e','v','i','c','e','\0','\0','\0',',','s','s','i','\0','\0','\0','\0','i','d','\0','\0','n','a','m','e','\0','\0','\0','\0',0,0,8,-82};
            OscMessage mes (data, sizeof(data));
            expect(mes.isEmpty() == false, "error parsing message");
            expect(mes.getAddress() == "/serialosc/device", "error in address");
            expect(mes.getNumberOfArguments() == 3, "error parsing arguments");
            expect(mes.getArgumentType(0) == OscMessage::StringType, "error parsing argument 1");
            expect(mes.getArgumentAsString(0) == "id", "error parsing argument 1");
            expect(mes.getArgumentType(1) == OscMessage::StringType, "error parsing argument 2");
            expect(mes.getArgumentAsString(1) == "name", "error parsing argument 2");
            expect(mes.getArgumentType(2) == OscMessage::Int32Type, "error parsing argument 3");
            std::cout << mes.getArgumentAsInt32(2);
            expect(mes.getArgumentAsInt32(2) == 2222, "error parsing argument 3");
        }
        
        // Test osc message (/test 1 2. hello 2. 1 bye) for multiple argument parsing // need to add blobs to this and bools
        {
            const char data[] = {   '/','t','e','s','t','\0','\0','\0',',','i','f','s','f','i','s','\0',0,0,0,1,64,0,0,0,'h','e','l','l','o','\0','\0','\0',64,0,0,0,0,0,0,1,'b','y','e','\0'};
            OscMessage mes (data, sizeof(data));
            expect(mes.isEmpty() == false, "error parsing message");
            expect(mes.getAddress() == "/test", "error in address");
            expect(mes.getNumberOfArguments() == 6, "error parsing arguments");
            expect(mes.getArgumentType(0) == OscMessage::Int32Type, "error parsing argument 1");
            expect(mes.getArgumentAsInt32(0) == 1, "error parsing argument 1");
            expect(mes.getArgumentType(1) == OscMessage::Float32Type, "error parsing argument 2");
            expect(mes.getArgumentAsFloat32(1) == 2.f, "error parsing argument 2");
            expect(mes.getArgumentType(2) == OscMessage::StringType, "error parsing argument 3");
            expect(mes.getArgumentAsString(2) == "hello", "error parsing argument 3");
            expect(mes.getArgumentType(3) == OscMessage::Float32Type, "error parsing argument 4");
            expect(mes.getArgumentAsFloat32(3) == 2.f, "error parsing argument 4");
            expect(mes.getArgumentType(4) == OscMessage::Int32Type, "error parsing argument 5");
            expect(mes.getArgumentAsInt32(4) == 1, "error parsing argument 5");
            expect(mes.getArgumentType(5) == OscMessage::StringType, "error parsing argument 6");
            expect(mes.getArgumentAsString(5) == "bye", "error parsing argument 6");
        }
        
        {   //test blob with size multiple of 8
            const char data[] = {'/','o','u','t','p','u','t','/','s','e','r','i','a','l','/','w','r','i','t','e','/','1','\0','\0',',','b','\0','\0',0,0,0,8,'H','i',' ','S','e','b','!','!'};
            OscMessage mes (data, sizeof(data));
            expect(mes.isEmpty() == false, "error on test for blob with size multiple of 8");
            expect(mes.getAddress() == "/output/serial/write/1", "error parsing blob address on test for blob with size multiple of 8");
            expect(mes.getNumberOfArguments() == 1, "error parsing blob argument on test for blob with size multiple of 8");
            expect(mes.getArgumentType(0) == OscMessage::BlobType, "error parsing blob argument on test for blob with size multiple of 8");
            
            const char text[] = "Hi Seb!!";
            expect(mes.getBlobSize(0) == sizeof(text) - 1, "error parsing blob size on test for blob with size multiple of 8");
            MemoryBlock blob (mes.getArgumentAsBlob(0), mes.getBlobSize(0));
            MemoryBlock compare (text, sizeof(text) - 1);
            expect(blob == compare, "error parsing blob contents on test for blob with size multiple of 8");
        }
        
        {   //test for blob with padding
            const char data[] = {'/','o','u','t','p','u','t','/','s','e','r','i','a','l','/','w','r','i','t','e','/','1','\0','\0',',','b','\0','\0',0,0,0,9,'H','i',' ','S','e','b','!','!','!','\0','\0','\0'};
            OscMessage mes (data, sizeof(data));
            expect(mes.isEmpty() == false, "error parsing blob message");
            expect(mes.getAddress() == "/output/serial/write/1", "error parsing blob address");
            expect(mes.getNumberOfArguments() == 1, "error parsing blob argument");
            expect(mes.getArgumentType(0) == OscMessage::BlobType, "error parsing blob argument");
            
            const char text[] = "Hi Seb!!!";
            expect(mes.getBlobSize(0) == sizeof(text) - 1, "error parsing blob size");
            MemoryBlock blob (mes.getArgumentAsBlob(0), mes.getBlobSize(0));
            MemoryBlock compare (text, sizeof(text) - 1);
            expect(blob == compare, "error parsing blob contents");
        }
        
        {   //no argument test
            const char data[] = {'/','t','e','s','t','\0','\0','\0',',','\0','\0','\0'};
            OscMessage mes (data, sizeof(data));
            expect(mes.isEmpty() == false, "address incorrectly reported as empty with zero argument test");
            expect(mes.getNumberOfArguments() == 0, "error testing zero argument test");
        }
        
        {   //bool argument test with 4 charlen typetag 
            const char data[] = {'/','t','e','s','t','\0','\0','\0',',','T','F','\0'};
            OscMessage mes (data, sizeof(data));
            expect(mes.isEmpty() == false, "error parsing two bool arguemnts");
            expect(mes.getNumberOfArguments() == 2, "error testing zero argument test");
            expect(mes.getArgumentType(0) == OscMessage::BoolType, "error parsing two bool arguemnts");
            expect(mes.getArgumentAsBool(0) == true, "error parsing two bool arguemnts");
            expect(mes.getArgumentType(1) == OscMessage::BoolType, "error parsing two bool arguemnts");
            expect(mes.getArgumentAsBool(1) == false, "error parsing two bool arguemnts");
            
        }
        
        {   //single integer argument test
            const char data[] = {'/','t','e','s','t','\0','\0','\0',',','i','\0','\0',0,0,0,1};
            OscMessage mes (data, sizeof(data));
            expect(mes.isEmpty() == false, "address only parsed ok with single integer argument test");
            expect(mes.getNumberOfArguments() == 1, "incorect number of arguments - single integer argument test");
            expect (mes.getArgumentType(0) == OscMessage::Int32Type, "incorect type - single integer argument test");
            expect (mes.getArgumentAsInt32(0) == 1, "incorrect value - single integer argument test");
        }
        
        {   //single float argument test
            const char data[] = {'/','t','e','s','t','\0','\0','\0',',','f','\0','\0',63,-128,0,0};
            OscMessage mes (data, sizeof(data));
            expect (mes.getArgumentType(0) == OscMessage::Float32Type, "incorect type - single float argument test");
            expect (mes.getArgumentAsFloat32(0) == 1.f, "incorrect value - single float argument test");
        }
        
        {   //single bool argument test
            const char data[] = {'/','t','e','s','t','\0','\0','\0',',','T','\0','\0'};
            OscMessage mes (data, sizeof(data));
            expect (mes.getArgumentType(0) == OscMessage::BoolType, "incorect type - single Bool argument test");
            expect (mes.getArgumentAsBool(0) == true, "incorrect value - single Bool argument test");
        }
        
        {   //single bool argument test
            const char data[] = {'/','t','e','s','t','\0','\0','\0',',','F','\0','\0'};
            OscMessage mes (data, sizeof(data));
            expect (mes.getArgumentType(0) == OscMessage::BoolType, "incorect type - single Bool argument test");
            expect (mes.getArgumentAsBool(0) == false, "incorrect value - single Bool argument test");
        }
        
        {   //empty argument test
            OscMessage mes;
            expect(mes.isEmpty() == true, "Checking empy");
            expect(mes.getAddress() == String::empty, "Checking empy");
        }
        
        {   //address constructor test
            OscMessage mes ("/new/address");
            expect(mes.getAddress() == "/new/address", "address constructor test");
        }
        
        {   //set address function test
            OscMessage mes;
            mes.setAddress("/new/address");
            expect(mes.getAddress() == "/new/address", "setAddress() function test");
        }
        
        
        
        beginTest ("Testing OscMessage parsing for message construction");
        // Test osc message (/test 1 2. hello 2. 1 bye) for multiple argument parsing // need to add blobs to this and bools
        {
            const char data[] = {   '/','t','e','s','t','\0','\0','\0',',','i','f','s','f','i','s','\0',0,0,0,1,64,0,0,0,'h','e','l','l','o','\0','\0','\0',64,0,0,0,0,0,0,1,'b','y','e','\0'};
            //OscMessage mes1 (data, sizeof(data));
            OscMessage mes2;
            
            mes2.setAddress("/test"); 
            mes2.addInt32Argument (1);
            mes2.addFloat32Argument (2.f);
            mes2.addStringArgument ("hello");
            mes2.addFloat32Argument (2.f);
            mes2.addInt32Argument (1);
            mes2.addStringArgument ("bye");
            
            expect(mes2.getNumberOfArguments() == 6, "error parsing arguments");
            expect(mes2.getArgumentType(0) == OscMessage::Int32Type, "error parsing argument 1");
            expect(mes2.getArgumentAsInt32(0) == 1, "error parsing argument 1");
            expect(mes2.getArgumentType(1) == OscMessage::Float32Type, "error parsing argument 2");
            expect(mes2.getArgumentAsFloat32(1) == 2.f, "error parsing argument 2");
            expect(mes2.getArgumentType(2) == OscMessage::StringType, "error parsing argument 3");
            expect(mes2.getArgumentAsString(2) == "hello", "error parsing argument 3");
            expect(mes2.getArgumentType(3) == OscMessage::Float32Type, "error parsing argument 4");
            expect(mes2.getArgumentAsFloat32(3) == 2.f, "error parsing argument 4");
            expect(mes2.getArgumentType(4) == OscMessage::Int32Type, "error parsing argument 5");
            expect(mes2.getArgumentAsInt32(4) == 1, "error parsing argument 5");
            expect(mes2.getArgumentType(5) == OscMessage::StringType, "error parsing argument 6");
            expect(mes2.getArgumentAsString(5) == "bye", "error parsing argument 6");
            
            MemoryBlock bloc1 (data, sizeof(data));
            MemoryBlock bloc2 (mes2.getRawData(), mes2.getRawDataSize());
            expect(bloc1 == bloc2, "data format error when constructing message");
            
        }
        
        {   //single bool argument test
            const char data[] = {'/','t','e','s','t','\0','\0','\0',',','T','\0','\0'};
            OscMessage mes ("/test");
            mes.addBoolArgument(true);
            expect (mes.getArgumentType(0) == OscMessage::BoolType, "incorect type - single Bool argument test");
            expect (mes.getArgumentAsBool(0) == true, "incorrect value - single Bool argument test");
            
            MemoryBlock bloc1 (data, sizeof(data));
            MemoryBlock bloc2 (mes.getRawData(), mes.getRawDataSize());
            expect(bloc1 == bloc2, "data format error when constructing message");
        }
    }
};


static OscMessageTest oscMessageTest;

#endif
