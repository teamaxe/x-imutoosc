//
//  OscControls.h
//  x-IMUtoOSC
//
//  Created by Tom Mitchell on 18/10/2015.
//
//

#ifndef H_OscControls
#define H_OscControls

#include "../../JuceLibraryCode/JuceHeader.h"
#include "../osc/OscConnection.h"

class OscControls : public Component, public TextEditor::Listener
{
public:
    OscControls (OscConnection& o) :  osc (o)
    {
        sendIpTextEditor.setText (IPAddress::local().toString());
        sendIpTextEditor.setTextToShowWhenEmpty ("Send IP address...", Colours::grey);
        sendIpTextEditor.addListener (this);
        addAndMakeVisible (sendIpTextEditor);
        textEditorFocusLost (sendIpTextEditor);
        
        sendPortTextEditor.setText ("8000");
        sendPortTextEditor.setTextToShowWhenEmpty ("Send port...", Colours::grey);
        sendPortTextEditor.addListener (this);
        addAndMakeVisible(sendPortTextEditor);
        textEditorFocusLost (sendPortTextEditor);
        
    }
    
    ~OscControls()
    {
    }
    
    const String& getIP()
    {
        return ipString;
    }
    
    int getPort()
    {
        return portNumber;
    }
    
    void textEditorFocusLost (TextEditor& te) override
    {
        if (&te == &sendIpTextEditor)
            ipString = sendIpTextEditor.getText();
        else
            portNumber = sendPortTextEditor.getText().getIntValue();
    }
    
    void resized()
    {
        Rectangle<int> r (getLocalBounds());
        sendPortTextEditor.setBounds (r.removeFromRight (r.getWidth()/3));
        sendIpTextEditor.setBounds (r);
    }

private:
    OscConnection& osc;
    TextEditor sendIpTextEditor;
    TextEditor sendPortTextEditor;
    
    String ipString;
    int portNumber;
};

#endif /* H_OscControls */
