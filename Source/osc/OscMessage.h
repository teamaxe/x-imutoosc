 /*
  ==============================================================================

    OscMessage.h
    Created: 8 Sep 2012 1:25:05pm
    Author:  tjmitche

  ==============================================================================
*/

#ifndef H_OSCMESSAGE
#define H_OSCMESSAGE

#include "../JuceLibraryCode/JuceHeader.h"

class OscMessage
{
public:

    /**
     Enum for argument types
     */
    enum ArgumentType
    {
        UnknownType,
        Int32Type,
        Float32Type,
        StringType,
        BlobType,
        
        BoolType,

        NumArgumentTypes
    };

    /**
     Union for OSC arguments
     */
    typedef union {
        int32 intValue;         //int part
        float floatValue;       //float part
        uint8 c[4];             // as bytes
    } Argument32;


    /** Constructor - creates an empty osc message - you need to check for white space in the osc address! */
    OscMessage (const String& address_ = String::empty) : address(address_)
    {}

    /** Constructor - creates an osc message from the raw data pointed to by message.
     if there's anything wrong with the rawdata format the message will be empty. */
    OscMessage (const char* const rawData, const int messageLength);
    
    /** Parses raw data into an OSC message. Returns false if successful and true if 
     an error occurs. In the future it would be nice to return an error code to indicate 
     what's wrong with the message source of the error */
    bool parseRawData (const char* const rawData, const int rawDataLength);

    /** Checks to see whether the message is empty */
    bool isEmpty() const;
    
    /** empties the message */
    void clear();
    
    /** Returns a string containing the entire message converted to a string */
    String asString();

    /** Returns actual reference to the address string. */
    const String& getAddress () const;

    /** Returns the number of arguments in the message */
    int getNumberOfArguments() const;

    /** Returns the type of the argument */
    ArgumentType getArgumentType(unsigned int index) const;
    
    /**
     Returns the specified argument as an int
     */
    int getArgumentAsInt32(unsigned int index) const;

    /**
     Returns the specified argument as a float
     */
    float getArgumentAsFloat32(unsigned int index) const;
    
    /**
     Returns the specified argument as a string
     */
    const String getArgumentAsString(unsigned int index) const;

    /** Returns the specified argument as a blob */
    const char* getArgumentAsBlob(unsigned int index) const;

    /** Returns the specified blob size, or 0 if this index is not a blob */
    int getBlobSize(unsigned int index) const;

    /** Returns the specified argument as a blob */
    bool getArgumentAsBool(unsigned int index) const;
    
    /**
     Returns the number of bytes required by the raw data for the current message.
     This is recalculated each time so that it always returns the required memory, the actual memory
     block is not allocated until @getRawData() is called.
     */
    const size_t getRawDataSize();

    /**
     Get rawData - allocates the memoryBlock and dumps the osc formatted
     message into it ready for sending and returns a pointer to this data.
     */
    const char* const getRawData();

    /**
     Set address - sets the address string
     */
    void setAddress (const String& newAddress);

    /**
     Add address - appends a new address string
     */
    void addAddress (const String& addAddress);

    /**
     Adds a float to the arguments
     */
    void addFloat32Argument (const float value);

    /**
     Adds a int to the arguments
     */
    void addInt32Argument (const int32 value);
    
    /**
     Adds a string to the arguments
     */
    void addStringArgument (const String& string);

    /**
     Adds a blob to the arguments
     */
    void addBlobArgument (const unsigned char* const blob, const unsigned int blobSize);
    
    /**
     Adds a bool to the arguments
     */
    void addBoolArgument (const bool state);

private:
    String address;                 //string for the address
    String typeTag;                 //string for the typetag
    Array <var> arguments;          //array of arguments
    MemoryBlock rawData;
};

#endif  // H_OSCMESSAGE


/* This was the start of my own varient class, but currently using juces var instead */
//
//    /**
//     Class for arguments
//     */
//    class Argument
//    {
//    public:
//        Argument() : type (UnknownType) {}
//        Argument(const int32 intValue) : type (Int32Type)  {arg32.intValue = intValue;}
//        Argument(const float floatValue) : type (Float32Type)  {arg32.floatValue = floatValue;}
//        Argument (const String& string) : type (StringType), argString (string) {}
//        Argument(const char* const blob, const unsigned int blobSize) : type (BlobType), argBlob (blob, blobSize)
//        {
//
////            argBlob.setSize (blobSize);
//
//            arg32.intValue = blobSize;
//
////            uint writePosition = 0; //write the size
////            argBlob[writePosition++] = arg32.c[3];
////            argBlob[writePosition++] = arg32.c[2];
////            argBlob[writePosition++] = arg32.c[1];
////            argBlob[writePosition++] = arg32.c[0];
////
////            argBlob.copyFrom(blob, writePosition, blobSize); //copy the blob
//
//        }
//
//        unsigned int getBlobSize() const
//        {
//            return type != BlobType ? 0 : arg32.intValue;
//        }
//
//        ArgumentType getType() const { return type; }
//
//    private:
//        ArgumentType type;
//
//    public:
//        Argument32 arg32;
//        MemoryBlock argBlob;
//        String argString;
//    };
