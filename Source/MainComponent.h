/*
  ==============================================================================

    This file was auto-generated!

  ==============================================================================
*/

#ifndef MAINCOMPONENT_H_INCLUDED
#define MAINCOMPONENT_H_INCLUDED

#include "../JuceLibraryCode/JuceHeader.h"
#include "SerialControls.h"
#include "xIMU/xImuSensor.h"
#include "osc/OscConnection.h"
#include "osc/OscControls.h"
#include "SensorButtons.h"

//==============================================================================
/* This component lives inside our window, and this is where you should put all
    your controls and content. */
class MainContentComponent :    public  Component,
                                public  xImuSensor::Listener,
                                public  Timer
{
public:
    //==============================================================================
    
    MainContentComponent (xImuSensor& x, OscConnection& o);
    ~MainContentComponent();

    //Component
    void paint (Graphics&) override;
    void resized() override;
    
    //ximu
    void xIMUDateTimeData (xImuSensor* source, DateTimeData* dataPacket) override {DBG("TimeData");}
    void xIMUCalBattThermData (xImuSensor* source, CalBattThermData* dataPacket) override {DBG("BattThermData");}
    void xIMUCommandData (xImuSensor* source, CommandData* dataPacket) override {DBG("CommandData");}
    void xIMURawBattThermData (xImuSensor* source, RawBattThermData* dataPacket) override {DBG("RawBattThermData");}
    void xIMUCalInertialMagData (xImuSensor* source, CalInertialMagData* dataPacket) override;
    void xIMUQuaternionData (xImuSensor* source, QuaternionData* dataPacket) override;
    void xIMUDigitalIOdata (xImuSensor* source, DigitalIOdata* dataPacket); 
    void xIMUErrorData (xImuSensor* source, ErrorData* dataPacket) override {DBG("ErrorData");}
    void xIMURawAnalogueData (xImuSensor* source, RawAnalogueInputData* dataPacket) override;
    void xIMURegisterData (xImuSensor* source, RegisterData* dataPacket) override {DBG("RegisterData");}
    
    //Timer
    void timerCallback();
    
private:
    xImuSensor& ximu;
    OscConnection& osc;
    
    SerialControls serialControls;
    OscControls oscControls;
    
    SpinLock lock;
    String streamText;
    Label streamTextLabel;
    
    ImuButtons imuButtons;
    InertialButtons inertialButtons;
    DigitalButtons digitalButtons;
    DigitalButtons analogueButtons;

    //==============================================================================
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (MainContentComponent)
};


#endif  // MAINCOMPONENT_H_INCLUDED
